--
-- Övning 01: Skapa en databas
--
-- Detta är en kommentar i SQL
--

-- Skapa databas
CREATE DATABASE skolan;

-- Välj vilken databas du vill använda
USE skolan;

-- Radera en databas
-- DROP DATABASE skolan;

--
-- Skapa tabell Lärare
--
CREATE TABLE larare
(
  akronym CHAR(3) PRIMARY KEY,
  avdelning CHAR(3),
  namn CHAR(20),
  lon INT,
  fodd DATETIME
);

-- Radera tabell
-- DROP TABLE larare

-- Välj allt innehåll i tabellen
SELECT * FROM larare;

--
-- Lägg till rader i tabellen Lärare
--
INSERT INTO larare VALUES ('MOS', 'APS', 'Mikael',   15000, '1968-03-07');
INSERT INTO larare VALUES ('MOL', 'AIS', 'Mats-Ola', 15000, '1978-12-07');
INSERT INTO larare VALUES ('BBE', 'APS', 'Betty',    15000, '1968-07-07');
INSERT INTO larare VALUES ('AJA', 'APS', 'Andreas',  15000, '1988-08-07');
INSERT INTO larare VALUES ('CJH', 'APS', 'Conny',    15000, '1943-01-07');
INSERT INTO larare VALUES ('CSA', 'APS', 'Charlie',  15000, '1969-04-07');
INSERT INTO larare VALUES ('BHR', 'AIS', 'Birgitta', 15000, '1964-02-07');
INSERT INTO larare VALUES ('MAP', 'APS', 'Marie',    15000, '1972-06-07');
INSERT INTO larare VALUES ('LRA', 'APS', 'Linda',    15000, '1975-03-07');
INSERT INTO larare VALUES ('ACA', 'APS', 'Anders',   15000, '1967-09-07');

-- Välj allt innehåll i tabellen
SELECT * FROM larare;

--
-- Radera rader från en tabell
--
DELETE FROM larare WHERE namn = 'Mikael';
DELETE FROM larare WHERE avdelning = 'AIS';
DELETE FROM larare LIMIT 2;
DELETE FROM larare;

-- Ändra befintlig tabell
ALTER TABLE larare ADD COLUMN kompetens INT;

-- Ta bort kolumn från tabell
ALTER TABLE larare DROP COLUMN kompetens;

-- Ändra befintlig tabell
ALTER TABLE larare ADD COLUMN kompetens INT NOT NULL DEFAULT 5;

--
-- Uppdatera ett värde
--
UPDATE larare SET namn = 'Charles' WHERE akronym = 'CSA';
UPDATE larare SET kompetens = 7, lon = 21000 WHERE namn = 'Mikael';
UPDATE larare SET lon = lon + 6000 WHERE namn = 'Mats-Ola';
UPDATE larare SET kompetens = 9, lon = 21000 WHERE namn = 'Betty';
UPDATE larare SET lon = lon - 1200 WHERE namn = 'Andreas';
UPDATE larare SET lon = lon * 1.1 LIMIT 10;


--
-- Uppgift 8.1
--
SELECT * FROM larare WHERE avdelning = 'AIS';
SELECT * FROM larare WHERE akronym LIKE 'M%';
SELECT * FROM larare WHERE namn LIKE '%o%';
SELECT * FROM larare WHERE lon >= 20000;
SELECT * FROM larare WHERE kompetens > 5 AND lon >= 20000;
SELECT * FROM larare WHERE akronym IN ('MOS', 'MOL', 'BBE');

--
-- Uppgift 8.2
--
SELECT namn, lon FROM larare;
SELECT namn, lon FROM larare ORDER BY namn;
SELECT namn, lon FROM larare ORDER BY namn DESC;
SELECT namn, lon FROM larare ORDER BY lon;
SELECT namn, lon FROM larare ORDER BY lon DESC;
SELECT namn, lon FROM larare ORDER BY lon DESC LIMIT 3;

--
-- Byt namn på kolumn
--
SELECT
 namn AS 'Lärare',
 avdelning AS 'Avdelning',
 lon AS 'Lön'
FROM larare;

-- 9.1
SELECT MAX(lon) AS 'Högsta lön' FROM larare;
SELECT MIN(lon) AS 'Lägsta lön' FROM larare;

-- 9.2
SELECT COUNT(namn) AS Antal, avdelning AS Avdelning FROM larare GROUP BY avdelning;
SELECT SUM(lon) AS 'Summa lön', avdelning AS Avdelning FROM larare GROUP BY avdelning;
SELECT AVG(lon) AS 'Medellön', avdelning AS Avdelning FROM larare GROUP BY avdelning;

-- 9.3

--
-- SQL för att visa de avdelningar där snittlönen är över 18 000
--
SELECT avdelning, AVG(lon) AS Medellon
FROM larare
GROUP BY avdelning
HAVING AVG(lon) > 18000;

--
-- SQL för att visa de vanligaste lönerna.
--
SELECT lon, COUNT(lon) AS Antal
FROM larare
GROUP BY lon
HAVING COUNT(lon) > 1;

-- 10.1
SELECT CONCAT_WS('/', avdelning, akronym) as 'Avdelning och akronym' FROM larare;
SELECT LOWER(CONCAT_WS('/', avdelning, akronym)) as 'Avdelning och akronym' FROM larare;

-- 10.2 Datum och tid
SELECT NOW() as Tiden;
SELECT CURDATE() as Datum;
SELECT namn, fodd, NOW() as 'Nuvarande tid' FROM larare;

-- 10.3 Beräkna ålder
SELECT namn as Namn, TIMESTAMPDIFF(YEAR, fodd, CURDATE()) AS 'Ålder' FROM larare;

-- 11.1
CREATE VIEW vLarare as SELECT namn, TIMESTAMPDIFF(YEAR, fodd, CURDATE()) AS 'alder' FROM larare;
SELECT * FROM vLarare;
SELECT AVG(alder) AS 'Medelålder' FROM vLarare;
DROP VIEW vLarare;

-- 11.2
CREATE VIEW vLarare2 as SELECT *, TIMESTAMPDIFF(YEAR, fodd, CURDATE()) AS 'alder' FROM larare;
SELECT avdelning, AVG(alder) as 'Medelålder' FROM vLarare2 GROUP BY avdelning;
SELECT avdelning, AVG(alder) as 'Medelålder', AVG(lon) as 'Medellön' FROM vLarare2 GROUP BY avdelning;
SELECT avdelning, ROUND(AVG(alder)) as 'Medelålder', ROUND(AVG(lon)) as 'Medellön' FROM vLarare2 GROUP BY avdelning;

-- 11.3
CREATE VIEW vAvdelningsRapport as SELECT avdelning, ROUND(AVG(alder)) as 'medelalder', ROUND(AVG(lon)) as 'medellon' FROM vLarare2 GROUP BY avdelning;
SELECT * FROM vAvdelningsRapport;

-- 12.1
CREATE TABLE kurs
(
  kod CHAR(6) PRIMARY KEY NOT NULL,
  namn CHAR(40),
  poang FLOAT,
) ENGINE = INNODB CHARACTER SET utf8;

CREATE TABLE kurstillfalle
(
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  kod CHAR(6) NOT NULL,
  akronym CHAR(3),
  lasperiod INT NOT NULL,
  FOREIGN KEY (kod) REFERENCES kurs(kod),
  FOREIGN KEY (akronym) REFERENCES larare(akronym)
) ENGINE = INNODB CHARACTER SET utf8;

-- 12.6
INSERT INTO kurs(kod, namn, poang) VALUES('DV1106', 'Databasteknik och Webbapps', 7.5);
INSERT INTO kurs(kod, namn, poang) VALUES('DV1219', 'Databasteknik', 7.5);
INSERT INTO kurs(kod, namn, poang) VALUES('PA1106', 'Individuellt Projekt', 7.5);

INSERT INTO kurstillfalle(kod, akronym, lasperiod) VALUES('DV1106', 'MOS', 1);
INSERT INTO kurstillfalle(kod, akronym, lasperiod) VALUES('DV1106', 'MOS', 4);
INSERT INTO kurstillfalle(kod, akronym, lasperiod) VALUES('DV1219', 'CJH', 2);
INSERT INTO kurstillfalle(kod, akronym, lasperiod) VALUES('DV1219', 'MOS', 3);
INSERT INTO kurstillfalle(kod, akronym, lasperiod) VALUES('PA1106', 'MOL', 1);
INSERT INTO kurstillfalle(kod, akronym, lasperiod) VALUES('PA1106', 'BBE', 2);

-- 12.7

--
-- En crossjoin
--
SELECT * FROM kurs, kurstillfalle;

--
-- Joina två tabeller, använd alias för att korta ned SQL-satsen
--
CREATE VIEW vKurstillfallen AS
SELECT k.kod, k.namn, k.poang, kt.akronym, kt.lasperiod
FROM kurs AS k, kurstillfalle AS kt
WHERE k.kod = kt.kod;

CREATE VIEW vKursinfo AS
SELECT vkt.kod, vkt.namn AS kursnamn, vkt.poang, vkt.lasperiod, vl.akronym, vl.avdelning, vl.namn, vl.lon, vl.fodd, vl.kompetens, vl.alder
FROM vKurstillfallen AS vkt, vLarare2 AS vl
WHERE vkt.akronym = vl.akronym;

SELECT kod, kursnamn, lasperiod, namn AS kursansvarig FROM vKursinfo;

-- 12.9

--
-- Inner join av samtliga tabeller.
--
SELECT
  k.kod AS Kurskod,
  k.namn AS Kursnamn,
  kt.lasperiod AS Läsperiod,
  CONCAT(l.namn, ' (', l.akronym, ')') AS Kursansvarig
FROM kurstillfalle AS kt
  INNER JOIN kurs AS k
    ON kt.kod = k.kod
  INNER JOIN larare AS l
    ON kt.akronym = l.akronym
 ORDER BY k.kod;

-- 13
SELECT AVG(alder) FROM vKursinfo WHERE kod = 'PA1106';
-- 13.1: medålddern är 40
SELECT AVG(lon) FROM vKursinfo WHERE kod = 'PA1106';
-- 13.2: medelönen är 23 100


-- 14

--
-- Hur många kurstillfällen har lärarna?
--
CREATE VIEW vAntalKATillfallen
AS
SELECT akronym, COUNT(akronym) AS Antal
FROM vKursinfo
GROUP BY akronym;

SELECT * FROM vAntalKATillfallen;
SELECT MAX(Antal) FROM vAntalKATillfallen;

-- SVAR = 3

SELECT *
FROM VVAntalKATillfallen
WHERE Antal = 3;

--
-- En fråga med en subquery
--
SELECT *
FROM vAntalKATillfallen
WHERE Antal = (SELECT MAX(Antal) FROM vAntalKATillfallen);

-- 14.1
SELECT *
FROM vAntalKATillfallen
WHERE Antal = (SELECT MIN(Antal) FROM vAntalKATillfallen);


-- 15

--
-- Skapa kurs utan kurstillfälle och gör inner join mot tabell för kurstillfällen.
--
INSERT INTO kurs VALUES ('DV1207', 'Db och Webb2', 7.5);
SELECT * FROM kurs;

SELECT
  K.kod AS Kurskod,
  K.namn AS Kursnamn,
  Kt.lasperiod AS Läsperiod
FROM kurstillfalle AS Kt
  RIGHT OUTER JOIN kurs AS K
    ON Kt.kod = K.kod
ORDER BY K.kod;
