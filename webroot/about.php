
<?php
/**
 * This is a Anjo pagecontroller.
 *
 */
// Include the essential config-file which also creates the $anjo variable with its defaults.
include(__DIR__.'/config.php');


// Do it and store it all in variables in the Anjo container.
$anjo['title'] = "Om";

$anjo['main'] = <<<HTML
<h1>Om Rental Movies</h1>
<p>
  Välkommen till Rental Movies.
  Nu kan du hyra film på ett enkelt och smidigt sätt. Beställ din film hemma och få det skickat direkt ner i brevlådan.
</p>
HTML;


// Finally, leave it all to the rendering phase of Anjo.
include(ANJO_THEME_PATH);
