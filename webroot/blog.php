<?php
/**
 * This is a Anjo pagecontroller.
 *
 */
// Include the essential config-file which also creates the $anjo variable with its defaults.
include(__DIR__.'/config.php');


// Get parameters
$slug = isset($_GET['slug']) ? $_GET['slug'] : null;


// Get content
$res = array();
$blog = new CBlog();
if($slug) {
  $res[] = $blog->get($slug);
} else {
  $res = $blog->getLatestsPosts(3);
}


// SQL debug
$debug = CDatabase::debug();


// Prepare content and store it all in variables in the Anax container.
$anjo['title'] = "Nyheter";
$anjo['main'] = null;


// Do it and store it all in variables in the Anjo container.
if(isset($res[0]) && !empty($res[0])) {
  // Loop through all posts
  foreach($res as $c) {
    // Set title if its a single post
    if($slug) $anjo['title'] = "{$c->title} | {$anjo['title']}";
    // Edit link
    $editLink = CUser::isAuthenticated() ? "<br><a class='btn btn-default btn-sm' data-toggle='modal' data-target='.modal-edit' href='edit.php?id={$c->id}'>Uppdatera inlägget</a> <a class='btn btn-danger btn-sm' href='#'>Ta bort</a>" : null;
    $anjo['modal'] = "edit.php?id={$c->id}";
    $anjo['main'] .= <<<HTML
<section>
  <article>
    <header>
      <h1><a href='blog.php?slug={$c->slug}'>{$c->title}</a></h1>
      <small>Postad: {$c->published} Av: {$c->author}</small>
    </header>
    {$c->data}
    <footer>
      {$editLink}
    </footer>
  </article>
</section>
HTML;
  }

  if(count($res) > 1) {
    $anjo['main'] .= <<<HTML
<ul class="pager">
  <li class="previous disabled"><a href="#">← Older</a></li>
  <li class="next"><a href="#">Newer →</a></li>
</ul>
HTML;
  }

}
else if($slug) {
  $anjo['main'] = "Det fanns inte en sådan bloggpost.";
}
else {
  $anjo['main'] = "Det fanns inga bloggposter.";
}


// Finally, leave it all to the rendering phase of Anjo.
include(ANJO_THEME_PATH);
