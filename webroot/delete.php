<?php
/**
 * This is a Anjo pagecontroller.
 *
 */
// Include the essential config-file which also creates the $anjo variable with its defaults.
include(__DIR__.'/config.php');


// Check if user is logged in, if not redirect them to login form
if(!CUser::isAuthenticated()) CRedirect::url('login.php');


// Get parameters
$id = isset($_POST['id'])    ? strip_tags($_POST['id']) : (isset($_GET['id']) ? strip_tags($_GET['id']) : null);
is_numeric($id) or die('Check: Id must be numeric.');


// Setup content object
$content = new CContent();


// Delete content
if($content->destroy($id, true)) {
  $alert = '<div class="alert alert-success">Innehållet togs bort.</div>';
  CRedirect::url('view.php?alert=' . urlencode($alert));
} else {
  $alert = '<div class="alert alert-danger">Innehållet kunde inte tas bort.</div>';
  CRedirect::url('view.php?alert=' . urlencode($alert));
}