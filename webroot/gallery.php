<?php
/**
 * This is a Anjo pagecontroller.
 *
 */
// Include the essential config-file which also creates the $anjo variable with its defaults.
include(__DIR__.'/config.php');



// Define the basedir for the gallery
define('GALLERY_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 'gallery');
define('GALLERY_BASEURL', 'gallery/');



// Get incoming parameters
$path = isset($_GET['path']) ? $_GET['path'] : null;



// Get gallery and breadcrumbs
$gallery = CGallery::get($path);
$breadcrumbs = CGallery::breadcrumbs($path);



// Do it and store it all in variables in the Anjo container.
$anjo['title'] = "Galleri";

$anjo['main'] = <<<HTML

<h1>{$anjo['title']}</h1>

$breadcrumbs
$gallery

HTML;


// Finally, leave it all to the rendering phase of Anjo.
include(ANJO_THEME_PATH);
