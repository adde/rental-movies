<?php
/**
 * This is a Anjo pagecontroller.
 *
 */
// Include the essential config-file which also creates the $anjo variable with its defaults.
include(__DIR__.'/config.php');


// Alerts
$alerts = null;


// Check actions
if(isset($_POST['submit'])) {
  if(!CUser::login($_POST['username'], $_POST['password'])) {
    $alerts = '<div class="alert alert-danger">Du angav fel inloggningsuppgifter. Försök igen.</div>';
  } elseif(isset($_GET['url'])) {
    CRedirect::url($_GET['url']);
  } else {
    CRedirect::url('./');
  }
}


$secret = (CUser::isAuthenticated()) ? '<p>Du är inloggad som <strong>' . CUser::getName() . '</strong>! <a href="logout.php">Vill du logga ut?</a></p>' : '';
$form = (CUser::isAuthenticated()) ? '' : <<<HTML
<div class="row">
  <form method="post" class="col-md-6">
    <fieldset>
    <legend>Logga in</legend>
    <div class="form-group">
      <label for="username">Användarnamn:</label>
      <input type="text" class="form-control" name="username" id="username" placeholder="Användarnamn">
    </div>
    <div class="form-group">
      <label for="password">Lösenord:</label>
      <input type="password" class="form-control" name="password" id="password" placeholder="Lösenord">
    </div>
    <div class="form-group">
      <input type="submit" name="submit" class="btn btn-primary" value="Logga in">
    </div>
    </fieldset>
  </form>
</div>
HTML;


// Do it and store it all in variables in the Anjo container.
$anjo['title'] = "Inloggning";

$anjo['main'] = <<<HTML
<h1>{$anjo['title']}</h1>

{$alerts}
{$secret}
{$form}

HTML;


// Finally, leave it all to the rendering phase of Anjo.
include(ANJO_THEME_PATH);