
<?php
/**
 * This is a Anjo pagecontroller.
 *
 */
// Include the essential config-file which also creates the $anjo variable with its defaults.
include(__DIR__.'/config.php');


// Do it and store it all in variables in the Anjo container.
$anjo['title'] = "Start";

$anjo['main'] = <<<HTML
<h1>Rental Movies</h1>
<p>
  Välkommen till Rental Movies.
  Nu kan du hyra film på ett enkelt och smidigt sätt. Beställ din film hemma och få det skickat direkt ner i brevlådan.
</p>
<p>
  Vi erbjuder en stor variation av filmer, från gamla klassiker till nyare filmer, filmer som passar alla.
  Du kan själv välja hur länge du vill hyra filmen från minst 1 månad upp till 12 månader, betala med faktura eller direktbetalning.
</p>
<h2>Nya filmer</h2>
<div class="row">
  <div class="col-sm-4"><img src="img.php?src=movies/godzilla.jpg&amp;width=230" alt="Godzilla"></div>
  <div class="col-sm-4"><img src="img.php?src=movies/dofp.jpg&amp;width=230" alt="X-men: Days of future past"></div>
  <div class="col-sm-4"><img src="img.php?src=movies/tas2.jpg&amp;width=230" alt="The amazing spider man 2"></div>
</div>
HTML;


// Finally, leave it all to the rendering phase of Anjo.
include(ANJO_THEME_PATH);
