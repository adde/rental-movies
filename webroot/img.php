<?php
/**
 * This is a PHP skript to process images using PHP GD.
 *
 */



// Set time zone
date_default_timezone_set('Europe/Stockholm');



// Include CImage
include(__DIR__.'/../src/CImage/CImage.php');



// Define some constant values, append slash
// Use DIRECTORY_SEPARATOR to make it work on both windows and unix.
define('IMG_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR);
define('CACHE_PATH', __DIR__ . '/cache/');



// Get the incoming arguments
$src          = isset($_GET['src'])           ? $_GET['src']        : null;
$verbose      = isset($_GET['verbose'])       ? true                : null;
$saveAs       = isset($_GET['save-as'])       ? $_GET['save-as']    : null;
$quality      = isset($_GET['quality'])       ? $_GET['quality']    : 60;
$ignoreCache  = isset($_GET['no-cache'])      ? true                : null;
$newWidth     = isset($_GET['width'])         ? $_GET['width']      : null;
$newHeight    = isset($_GET['height'])        ? $_GET['height']     : null;
$cropToFit    = isset($_GET['crop-to-fit'])   ? true                : null;
$sharpen      = isset($_GET['sharpen'])       ? true                : null;

$pathToImage = realpath(IMG_PATH . $src);



// Output image with provided settings
CImage::make([
  'verbose'       => $verbose,
  'src'           => $src,
  'pathToImage'   => $pathToImage,
  'saveAs'        => $saveAs,
  'quality'       => $quality,
  'ignoreCache'   => $ignoreCache,
  'newWidth'      => $newWidth,
  'newHeight'     => $newHeight,
  'cropToFit'     => $cropToFit,
  'sharpen'       => $sharpen,
]);