<?php
/**
 * This is a Anjo pagecontroller.
 *
 */
// Include the essential config-file which also creates the $anjo variable with its defaults.
include(__DIR__.'/config.php');


// Check if user is logged in, if not redirect them to login form
if(!CUser::isAuthenticated()) CRedirect::url('login.php?url=' . CUtil::currentUrl());


// Check if form was submitted
$alert = null;
if(isset($_POST['save'])) {
  $content = new CContent();
  if($content->store($_POST)) {
    $alert = '<div class="alert alert-success">Innehållet sparades.</div>';
    CRedirect::url('view.php?alert=' . urlencode($alert));
  } else {
    $alert = '<div class="alert alert-warning">Ett fel uppstod. Innehållet sparades INTE.</div>';
  }
}


// Get Filter select
$filtersSelect = '<select class="form-control" name="filter[]" multiple="multiple" size="'. count(CTextFilter::$options) . '">';
foreach (CTextFilter::$options as $key => $value) {
  $filtersSelect .= '<option value="'.$key.'">'.$key.'</option>';
}
$filtersSelect .= '</select>';


// Get type select
$typeSelect = '<select class="form-control" name="type">';
$typeSelect .= '<option value="page">Page</option>';
$typeSelect .= '<option value="post">Post</option>';
$typeSelect .= '</select>';


// Database queries
$debug = CDatabase::debug();


// Date and time
$now = date("Y-m-d H:i:s");


// Check if database table exists
$dbExists = !CContent::exists() ? '<div class="alert alert-warning">Det finns ingen databastabell för innehåll, vänligen <a href="view.php?init">initiera</a> den innan du försöker skapa innehåll.</div>' : null;


// Do it and store it all in variables in the Anjo container.
$anjo['title'] = "Skapa innehåll";

$anjo['main'] = <<<HTML

<h1>{$anjo['title']}</h1>

{$dbExists}
{$alert}

<form method=post>
  <fieldset>
  <legend>Skapa innehåll</legend>

  <div class="form-group">
    <label>Titel:</label>
    <input class="form-control" type='text' name='title' value=''/>
  </div>
  <div class="form-group">
    <label>Url:</label>
    <input class="form-control" type='text' name='url' value=''/>
  </div>
  <div class="form-group">
    <label>Text:</label>
    <textarea class="form-control" name='data' rows='10'></textarea>
  </div>
  <div class="form-group">
    <label>Type:</label>
    {$typeSelect}
  </div>
  <div class="form-group">
    <label>Filter:</label>
    {$filtersSelect}
  </div>
  <div class="form-group">
    <label>Publiceringsdatum:</label>
    <input class="form-control" type='text' name='published' value='{$now}'/>
  </div>
  <div class="form-group">
    <input class="btn btn-success" type='submit' name='save' value='Spara'/>
  </div>
  </fieldset>
</form>

<hr>

<h3>SQL</h3>
<pre>{$debug}</pre>

HTML;


// Finally, leave it all to the rendering phase of Anjo.
include(ANJO_THEME_PATH);
