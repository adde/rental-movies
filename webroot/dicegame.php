<?php
/**
 * This is a Anjo pagecontroller.
 *
 */
// Include the essential config-file which also creates the $anjo variable with its defaults.
include(__DIR__.'/config.php');

$dg = new DiceGame();

if(isset($_GET['reset'])) {
  $dg->reset();
}

if(isset($_GET['roll'])) {
  $dg->play();
}

if(isset($_GET['secure'])) {
  $dg->secure();
}

$roll = $dg->round->get_current_roll();
$points = $dg->round->get_total();
$points_secured = $dg->round->get_secured_points();
$rolls = $dg->round->get_total_rolls();

$buttons = [
  'roll' => '<a href="?roll" class="btn btn-primary">Kasta tärning</a>',
  'secure' => '<a href="?secure" class="btn btn-primary">Spara poäng</a>',
  'reset' => '<a href="?reset" class="btn btn-primary">Starta om</a>'
];

if($dg->won(100)) {
  $win = '<p><strong>Grattis, du vann! :)</strong></p>';
  $buttons['roll'] = '';
  $buttons['secure'] = '';
} else {
  $win = '';
}

// Do it and store it all in variables in the Anjo container.
$anjo['title'] = "Tävling";

$anjo['main'] = <<<HTML
<h1>{$anjo['title']}</h1>
$win
<p>Du kastade: $roll</p>
<p>Poäng: $points</p>
<p>Sparade poäng: $points_secured</p>
<p>Totalt antal kast: $rolls</p>
<p>{$buttons['roll']} {$buttons['secure']} {$buttons['reset']}</p>
HTML;


// Finally, leave it all to the rendering phase of Anjo.
include(ANJO_THEME_PATH);
