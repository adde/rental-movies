<?php
/**
 * This is a Anjo pagecontroller.
 *
 */
// Include the essential config-file which also creates the $anjo variable with its defaults.
include(__DIR__.'/config.php');


// Get parameters
$url = isset($_GET['url']) ? $_GET['url'] : null;


// Get content
$page = new CPage();
$p = $page->get($url);
if($p) {
  $data = $page->getData();
  $title = $page->getTitle();
} else {
  die('Misslyckades: det finns inget innehåll.');
}
$editLink = CUser::isAuthenticated() ? "<br><a class='btn btn-default' href='edit.php?id={$p->id}'>Uppdatera sidan</a>" : null;


// SQL debug
$debug = CDatabase::debug();


// Do it and store it all in variables in the Anjo container.
$anjo['title'] = $title;
$anjo['main'] = <<<HTML

<article>
  <header>
    <h1>{$title}</h1>
  </header>
    {$data}
  <footer>
    {$editLink}
  </footer>
</article>

<hr>

<h3>SQL</h3>
<pre>{$debug}</pre>

HTML;


// Finally, leave it all to the rendering phase of Anjo.
include(ANJO_THEME_PATH);
