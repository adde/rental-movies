<?php
/**
 * This is a Anjo pagecontroller.
 *
 */
// Include the essential config-file which also creates the $anjo variable with its defaults.
include(__DIR__.'/config.php');


// Do it and store it all in variables in the Anjo container.
$anjo['title'] = "Redovisningar";

$anjo['main'] = <<<HTML
<h1>Redovisning</h1>
<h2>Kmom01: Kom igång med programmering i PHP</h2>
<p>
  <strong>Vilken utvecklingsmiljö använder du?</strong><br>
  Oftast använder jag min workstation med Windows 8.1 när jag arbetar.
  Det händer även att jag använder min Macbook Pro för utveckling när jag är på resande fot.
  Jag utgår alltid från utvecklingsmilön som ligger på en Ubuntu Server med Apache, PHP, MySQL installerat.
  Samba används för att dela ut www-mappen till min Windows-dator.
  Editerar filer gör jag i Sublime Text.
  Har aldrig kollat efter en ny editor sen jag hittade ST, den gör allt jag behöver och lite till.
  Övriga verktyg jag använder när jag utvecklar: Chrome, PuTTY, Git, FileZilla.
</p>
<p>
  <strong>Berätta hur det gick att jobba igenom guiden “20 steg för att komma igång PHP”, var något nytt eller kan du det?</strong><br>
  Hade redan snabbt tittat igenom guiden i förra kursen men gick igenom rubrikerna igen för säkerhets skull.
  Det var inga konstigheter den här gången heller.
  Förövrigt tycker jag det är en bra guide som täcker det mesta av grunderna inom PHP och är säkert nyttig för nybörjare.
</p>
<p>
  <strong>Vad döpte du din webbmall Anax till?</strong><br>
  Jag döpte min webbmall till Anjo, första två bokstäverna i mitt för- och efternamn.
  Dålig fantasi kan man lugnt säga.
  Jag började med att forka Anax-base för att sen döpa om den på Github.
  Efter det klonade jag ner en kopia och gjorde en "search and replace" på alla strängar som matchade "Anax".
  Till sist commitade jag ändringarna och pushade dem till mitt forkade repository.
</p>
<p>
  <strong>Vad anser du om strukturen i Anax, gjorde du några egna förbättringar eller något du hoppade över?</strong><br>
  För enklare sidor(som den här) fungerar Anax helt okej.
  Det jag inte riktigt gillar är att man skickar HTML från sidcontrollern/config till vyn.
  Egentligen borde man bara skicka vidare datan för att sen låta templaten/vyn ta hand om all HTML, CSS och JS.
  För att det ska fungera krävs det en del ändringar kring templaten och då skulle nog webbmallen mer likna ett mini MVC-ramverk.
  Bestämde mig ändå att hålla mig till mallen som den var och inte sväva ut för mycket.
</p>
<p>
  Jag valde att inkludera bootstrap för att slippa styla alla element själv.
  Eftersom bootstrap använder en hel del CSS3 och därmed browserspecifika prefix blir det omöjligt att validera CSS till hundra procent.
  De fel som genereras av browserspecifika prefix påverkar inte sidans funktionalitet och spelar därför ingen roll.
</p>
<p>
  <strong>Gick det bra att inkludera source.php? Gjorde du det som en modul i ditt Anax?</strong><br>
  Det gick helt felfritt att lägga in källkodsvisningen på sidan.
  Först klonade jag ner <code>CSource</code> från github direkt ner i mappen <code>Anjo/src</code>.
  Därmed blev <code>CSource</code> en modul i webbmallen.
  Sen sneglade jag på mos <code>source.php</code> och lade själv till <code>.gitignore</code> i arrayen för filer som ska exkluderas från källkodsvisningen.
</p>
<p>
  <strong>Gjorde du extrauppgiften med GitHub?</strong>
  Eftersom jag gjorde en fork av Anax-base var det bara att commita och pusha ändringarna så låg allt uppe på Github.
  Min webbmall finns här: <a href="https://github.com/adde/Anjo">https://github.com/adde/Anjo</a>
</p>
<h2>Kmom02: Objektorienterad programmering i PHP</h2>
<p>
  <strong>Hur väl känner du till objektorienterade koncept och programmeringssätt?</strong><br>
  Trevligt att äntligen få börja arbeta objektorienterat.
  Jag tror jag känner till det mesta gällande OOP och konceptet.
  Har programmerat lite C++ och Java så har med mig "tänket" där ifrån.
  Med det sagt finns det självklart delar jag inte har så bra koll på och behöver lära mig mer av.
  Själva momentet i sig gick väldigt smärtfritt faktiskt.
  Skrev merparten av koden för alla klasser på en gång och näst intill allt fungerade direkt.
  Hade dock lite problem med autoloadern då jag ville ha alla klasser som tillhör tärningsspelet i samma modulmapp.
  Det krävdes därför en liten justering av autoloadern för att alla klasser skulle laddas in korrekt.
</p>
<p>
  <strong>Jobbade du igenom oophp20-guiden eller skumläste du den?</strong><br>
  Jag skumläste mest igenom guiden för att se om det fanns något nytt för mig.
  En ny sak som jag inte har tänkt på innan är att man kan spara objekt i sessionen.
  Det hade jag användning av lite senare i momentet när jag skulle göra tärningsspelet.
  Egentligen skiljer det sig inte något från att spara en vanlig variabel i sessionen men det är ändå inte helt självklart.
</p>
<p>
  <strong>Berätta om hur du löste uppgiften med tärningsspelet 100, hur tänkte du och hur gjorde du, hur organiserade du din kod?</strong><br>
  Började med att använda tärningsklassen <code>Dice</code> från oophp20 och modifierade den efter mina behov.
  Gjorde sedan en klass <code>Round</code> som håller koll på statistik för varje spelrunda.
  Objektet sparar ner senaste kast, poäng, sparade poäng och antal kast.
  Egenskaperna kan bara ändras av objektet själv då de är privata.
  För att komma åt dem finns ett antal get-metoder.
  Behövde sedan en klass för att hantera spelet över sessionen så jag skapade <code>DiceGame</code>.
  Vid initiering sparas ett nytt objekt av <code>Round</code> i sessionen, alternativt om sessionen redan finns så används det befintliga objektet.
  <code>DiceGame</code> håller även koll på när en runda är färdigspelad.
  Gjorde till sist en webbsida för att visa tärningsspelet.
  Poäng, sparade poäng och antal kast skrivs ut efter varje kastad tärning.
  På sidan finns tre knappar som man använder för att spela.
  De är ganska självförklarande men "Kasta tärning" utför ett kast, "Spara poäng" sparar de nuvarande poängen och låter en fortsätta från den poängen om man får 1.
  Klickar man på "Starta om" nollställs rundan och man kan börja om från början.
  Jag valde att inte göra några av extrauppgifterna pga tidsbrist.
</p>
<p>
  <strong>Berätta om hur du löste uppgiften med Månadens Babe, hur tänkte du och hur gjorde du, hur organiserade du din kod?</strong><br>
  Gjorde inte den här uppgiften.
</p>
<h2>Kmom03: SQL och databasen MySQL</h2>
<p>
  <strong>Är du bekant med databaser sedan tidigare? Vilka?</strong><br>
  Jag har arbetat ganska mycket med databaser innan så bekant tror jag nog jag är.
  Första databasen jag kom i kontakt med var <em>Access</em> i en kurs på gymnasiet.
  När jag väl började lära mig PHP blev det självklart <em>MySQL</em> och det är nog den databas jag är mest bekväm med.
  Skillnaden mellan databaser är dock inte så stor, kan man bara konceptet tror jag det är ganska enkelt att hoppa över till en annan.
  Inom mitt arbete jobbar jag med webbapplikationer som använder sig av både <em>MySQL</em> och <em>Microsoft SQL Server</em>.
  För inte så länge sedan lärde jag mig en ny databas, <em>SQLite</em> i HTMLPHP-kursen.
</p>
<p>
  <strong>Hur känns det att jobba med MySQL och dess olika klienter, utvecklingsmiljö och BTH driftsmiljö?</strong><br>
  Det känns precis som hemma för mig.
  Som jag skrev ovan är <em>MySQL</em> den databas jag har mest erfarenhet av och därmed inte så mycket nytt för mig.
  Angående de olika klienterna använder jag mest <em>phpMyAdmin</em> och <em>terminalen</em>.
  <em>Terminalen</em> är smidig om man t.ex. snabbt behöver exportera/importera på en produktionsserver om man bara har tillgång till SSH.
  Den är dock lite mindre smidig om man ska börja hantera en massa data, då föredrar jag <em>phpMyAdmin</em>.
  <em>Workbench</em> är helt klart ett väldigt kraftfullt verktyg som kan komma till användning ibland.
  Jag vet dock med mig själv att jag sällan använder några av de unika funktioner i <em>Workbench</em>.
  Nöjer mig därför med <em>terminalen</em> och <em>phpMyAdmin</em> som täcker upp mina behov.
</p>
<p>
  <strong>Hur gick SQL-övningen, något som var lite svårare i övningen, kändes den lagom?</strong><br>
  SQL-övningen tycker jag var riktigt bra.
  Kör man igenom hela guiden och förstår allt där har man en väldigt bra SQL-grund att stå på.
  Det mesta handlar om att få syntaxen rätt då språket i sig är ganska självförklarande tycker jag.
  Eftersom jag är van MySQL:are var det ganska "straight forward" för mig.
  Stötte dock på några nyheter.
  <code>HAVING</code> har jag aldrig hört talas om innan så det var nytt för mig.
  Vyer är en del som jag har koll på men inte jobbat så mycket med.
  Det är engentligen inga konstigheter med vyer men det var bra att få en rejäl genomkörare av det.
  Blir lite lättare att hantera datan med vyer när man har för många och för stora selects.
</p>
<p>
  Här är mitt resultat från övningen "Kom igång med SQL": <a href="kom-igang-med-sql.sql">kom-igang-med-sql.sql</a>.
</p>
<h2>Kmom04: PHP PDO och MySQL</h2>
<p>
  <strong>Hur kändes det att jobba med PHP PDO?</strong><br>
  Det kändes alldeles utmärkt!
  Har använt PDO ganska mycket innan så det var precis som att vara hemma.
  Det blev ju inte överdrivet mycket PDO, inte mer än i CDatabase.
  I resten av koden var det ju självklart bättre att använda wrappern än att skriva rena PDO statments.
</p>
<p>
  Jag började med att skriva kod för klassen <code>CDatabase</code> eftersom den skulle ligga till grund för både användarmodulen och tabellmodulen.
  Jag valde att göra klassen och dess metoder statiska för att man ska slippa instansiera ett objekt varje gång man vill ansluta till databasen.
  Lade till konfiguration för databasen i min <code>\$anjo</code> variabel.
  Sedan var det bara att köra <code>CDatabase::connect(\$anjo['database'])</code> i valfri sidkontroller för att få tillgång till databasen.
  En select med wrappern kan se ut som följer <code>\$res = CDatabase::select("SELECT * FROM Movie");</code> där <code>\$res</code> är en array med resultatet av queryn.
</p>
<p>
  Klassen för användarhantering <code>CUser</code> blev väldigt enkel och har de funktioner som fanns med i specifikationen för uppgiften.
  Metoderna i klassen är ganska självförklarande och dessutom väldigt korta, precis som det ska vara.
  Jag gjorde sedan en sidkontroller med ett enkelt formulär som postar till sig själv.
  Om man anger rätt uppgifter sparas användarobjektet i sessionen och en välkomstsida visas med en länk till logga ut.
  Om man anger fel uppgifter skrivs ett felmeddelande ut.
  Klickar man på logga ut, förstörs sessionen och användaren skickas till <code>login.php</code> igen.
</p>
<p>
  <strong>Gjorde du guiden med filmdatabasen, hur gick det?</strong><br>
  Jag började med att läsa igenom guiden och min tanke var först att göra allt i guiden men sen fick jag lite ont om tid så jag hoppade över det och gick direkt till uppgifterna istället.
  Jag använde några delar från guiden till uppgifterna så som SQL för att skapa upp Movie och User tabellerna i databasen.
  Använde även koden från den samlade söksidan för att bygga min tabellhanterare.
  Är väldigt nöjd med hur lite kod det blev i sidkontrollern för filmarkivet.
</p>
<p>
  <strong>Du har nu byggt ut ditt Anax med ett par moduler i form av klasser, hur tycker du det konceptet fungerar så här långt, fördelar, nackdelar?</strong><br>
  Jag tycker det fungerar ganska bra överlag.
  Det trevliga med Anax är enkelheten att lägga till moduler.
  Det är bara att skapa en klass i src-mappen, sen är den klassen automatiskt tillgänglig(med hjälp av autoloadern) och klar att använda i sidkontrollern.
  En annan bra sak är exception-hanteraren som fångar upp exceptions och skriver ut stack tracen.
  Blir mycket lättare att felsöka.
  Den är i och för sig inte unik för just Anax men en väldigt användbar funktion i vilket fall.
</p>
<p>
  Det finns dock några brister som jag tror jag har tagit upp i ett tidigare moment men jag antar att de är värda att nämna igen.
  Jag är inget stort fan av heredoc och tycker det är väldigt otympligt att skicka en massa HTML i en enda variabel till templaten.
  Det optimala vore om man bara skickade den råa datan till templaten som sedan tar hand om att skriva ut datan med rätt HTML.
  Det går ju tyvärr inte i Anax fall då vi har en gemensam template för alla sidor.
  Separation mellan logik och presentation är en viktig aspekt(om inte den viktigaste) när man strukturerar sin kod.
</p>
<p>
  Jag läste igenom extrauppgifterna men valde att inte göra dem eftersom huvuduppgifterna tog längre tid än vad jag hade räknat med.
  Jag kanske får möjlighet att göra dem i ett senare moment?
</p>
<h2>Kmom05: Lagra innehåll i databasen</h2>
<p>
  <strong>Det blir en del moduler till ditt Anax nu, hur känns det?</strong><br>
  Det känns bra.
  Smidigt att bara slänga dit en klass utan att behöva tänka på att inkludera den.
  Positivt att ha stora delar av koden separerade från varandra.
  Det blir både lättare att underhålla och felsöka.
  I det här momentet lade jag lade till två egna moduler/klasser:
</p>
<ul>
  <li><code>CUtil</code> som är tänkt att samla alla nyttfunktioner som t.ex. dump och sanitize.</li>
  <li><code>CRedirect</code> som hanterar redirects.</li>
</ul>
<p>
  Jag uppdaterade även databasmodulen från förra momentet med en metod för att kunna debugga lättare.
  Den nya metoden samlar alla queries per request och returnerar dem som en sträng.
  I sidkontrollern är det sen bara att skriva <code>CDatabase::debug()</code> för att få ut de queries som har körts.
</p>
<p>
  <strong>Berätta hur du tänkte när du löste uppgifterna, hur tänkte du när du strukturerade klasserna och sidkontrollerna?</strong><br>
  Jag började med att jobba igenom guiden.
  Skapade först upp en databastabell från skriptet i guiden.
  För att kunna spara vilken användare som redigerar innehållet lade jag till en kolumn author i content-tabellen.
  Jag gjorde sedan klassen <code>CContent</code> och lade till de metoder som behövdes för att matcha kraven, <code>init()</code>, <code>store()</code>, <code>edit()</code>, <code>destroy()</code> och <code>all()</code>.
  För init-funktionen provade jag först med att lägga all sql i en sträng men fick då problem med citat-tecken.
  Skapade därför en egen sql-fil i samma mapp som sedan läses in med <code>file_get_contents()</code> i init-metoden.
  I efterhand en mycket snyggare lösning än den jag tänkte först.
  I CRUD-metoderna blev det få rader kod då de egentligen bara agerar mellanhand till databas-modulen.
</p>
<p>
  Jag gick sedan igenom sidkontrollerna i guiden och flyttade över de delar som jag ansåg passade bättre i <code>CContent</code> istället.
  Jag flyttade generering av url, sanering av data innan output och förberedelse av data innan insert/update i databasen.
  Jag bestämde mig för att inte flytta generering av HTML(listor och formulär) till modulerna då det känns bättre att ha det separerat från logiken.
  Blir smidigare att ändra HTML om den inte är omgiven av en massa kod.
</p>
<p>
  Klasserna <code>CPage</code> och <code>CBlog</code> blev väldigt enkla och lika varandra.
  Båda två ärver all funktionalitet från <code>CContent</code>.
  Utöver det så sanerar de sin data innan den returneras för utskrift.
  Enda skillnaden mellan de två är att <code>CBlog</code> har en egen metod för att hämta senaste blogginlägg.
</p>
<p>
  För TextFilter-klassen la jag bara ín alla metoder som static och flyttade ut mappnings-arrayen som en medlemsvariabel för att använda den när jag skapar selects för create/edit-formulären.
  Laddade även ner <code>HTMLPurify</code> och lade till den som ett filtreringsalternativ.
</p>
<p>
  <strong>Börjar du få en känsla för hur du kan strukturera din kod i klasser och moduler, eller kanske inte?</strong><br>
  Tycker jag har rätt bra koll.
  Ibland kan det vara lite knepigt att tänka ut strukturen om man inte har så mycket att gå på.
  I det här fallet fanns det inga större frågetecken.
</p>
<p>
  <strong>Snart har du grunderna klara i ditt Anax, grunderna som kan skapa många webbplatser, är det något du saknar så här långt, kanske några moduler som du känner som viktiga i ditt Anax?</strong><br>
  Det beror lite på hur avancerat man vill ha det.
  För att sätta upp en väldigt enkelt site med bara några sidor och enkelt innehåll rakt upp och ner så är det inte mycket som fattas.
  Det finns dock en hel del funktionalitet som man skulle kunna lägga till i bloggen t.ex. kommentarer, taggar m.m.
</p>
<p>
  Jag hann inte med att lägga till stöd för undermenyer i förra momentet, men nu blev det av.
  Kände att jag behövde det då det börjar bli fullt i huvudmenyn.
  Gick ganska snabbt när jag väl satte igång.
  Stilmässigt fanns allt jag behövde i bootstrap så det var i princip bara att slänga på ett par css klasser på ankare och menylista.
  Lade sedan till sub-items i konfigurationen för att kunna loopa ut dem innuti huvud-loopen.
  Jag tyckte att det räckte med en nivå i det här fallet.
  Om den skulle haft oändligt med nivåer hade man behövt skriva en rekursiv funktion istället.
</p>
<h2>Kmom06: Bildbearbetning och galleri</h2>
<p>
  <strong>Hade du erfarenheter av bildhantering sedan tidigare?</strong><br>
  Ja, bildhantering rent allmänt i program som t.ex. Photoshop har jag sysslat med innan.
  Jag har även jobbat lite grand med PHP GD för att skala ner och beskära bilder vid exempelvis uppladdning till ett CMS.
  Jag har däremot inte gjort något mer avancerat än så.
  Det verkar gå att få en hel del olika effekter med de filter som finns.
  Positivt att det finns möjligheter om man skulle behöva göra mer avancerad redigering på serversidan.
</p>
<p>
  <strong>Hur känns det att jobba i PHP GD?</strong><br>
  Det är inga konstigheter.
  Det finns ju som sagt massvis med funktioner i PHP GD att använda sig av när man redigerar bilder på serversidan.
  Det är omöjligt att komma ihåg definitionerna för alla funktionerna så man får ha dokumentationen nära till hands när man arbetar.
</p>
<p>
  Själva uppgiften i sig gick bra.
  Jag gjorde enligt instruktionerna och satte upp en <code>img.php</code> med all kod från guiden.
  Först testade jag att den koden fungerade.
  Sedan började jag med att flytta över funktioner till klassen/modulen <code>CImage</code>.
  Jag tycker att <code>img.php</code> från guiden redan var bra strukturerad, därför var det väldigt enkelt att få till klassens metoder och variabler.
  Jag lade till en metod <code>CImage::make()</code> som samlar in alla inställningar, slår ihop dem med standardinställningarna och kör alla nödvändiga metoder i rätt ordning.
  Det här blir ändpunkten som man använder i <code>img.php</code> för att köra hela bildhanteringen.
</p>
<p>
  När <code>CImage</code> var klar för att testas märkte jag att det var problem med cachningen.
  Undersökte först koden i modulen men hittade inga fel där.
  Tittade sen runt lite i forumet och hittade en tråd med liknande problem.
  Det visade sig att man inte kan köra <code>session_start()</code> om man vill cacha bilderna med 304.
  Jag hade inkluderat <code>config.php</code> precis som i andra sidkontroller.
  Tog därför bort det och inkluderade istället <code>CImage</code> manuellt.
  Ett följdfel på det blev sedan att <code>strtotime()</code> gav ett felmeddelande på skolans server eftersom den inte verkar ha någon time zone satt(?).
  Kopierade då över följande rad från <code>config.php</code> till img.php: <code>date_default_timezone_set('Europe/Stockholm');</code> för att lösa det problemet.
</p>
<p>
  Jag gjorde även extrauppgifterna för <code>CImage</code> med stöd för transparenta bilder och hantering av gif.
  Det var inga problem att göra och var snabbt avklarat.
</p>
<p>
  <strong>Hur känns det att jobba med img.php, ett bra verktyg i din verktygslåda?</strong><br>
  Jag är väldigt positiv till <code>img.php</code>.
  Den är väldigt smidig att arbeta med när man snabbt behöver ändra storlek eller beskära en bild för bestämda dimensioner.
  Jag har inte använt någon liknande teknik för bildhantering innan.
  Det är definitivt något jag kommer ha användning av i fortsättningen.
  Skulle jag implementera detta i en riktig sida hade jag nog kikat på något av biblioteken som länkades i beskrivningen då de förmodligen är mer beprövade än den egna <code>CImage</code>.
</p>
<p>
  <strong>Detta var sista kursmomentet innan projektet, hur ser du på ditt Anax nu, en summering så här långt?</strong><br>
  Jag tycker det har flutit på bra med utvecklingen av Anax(Anjo) från start.
  Bra tänk med att hålla mycket av koden i modulerna för att få så rena sidkontroller som möjligt.
  Grunden för ett väldigt enkelt CMS finns i mallen i form av <code>CContent</code> och <code>CUser</code>.
  Det finns dock inget sätt att få ut en meny av de sidor som sparas i databasen, därför lade jag till en metod i <code>CNavigation</code> för att hantera det.
  Nu återstår det bara att se om allt i mallen håller för projektet.
</p>
<p>
  <strong>Finns det något du saknar så här långt, kanske några moduler som du känner som viktiga i ditt Anax?</strong><br>
  Nej, jag kan inte komma på något specifikt som fattas.
  Som jag sa i ett tidigare moment beror det helt på vad man vill att Anax ska göra åt en.
  Som en enkel sida med innehåll rakt upp och ner räcker det.
  Antar att jag kommer märka om det är något som saknas när jag börjar med projektet.
</p>
<p>
  Enligt mig är framförallt <code>CDatabase</code> viktig men även <code>CNavigation</code> och <code>CUtil</code> behövs för att sidan ska gå att navigera.
  Övriga moduler är mer specifika för varje sida, därför jag tycker inte att de är nödvändiga för mallen i sig.
  Möjligtvis även <code>CContent</code> och <code>CPage</code> om man vill bygga mallen helt och hållet på dynamiska sidor.
</p>
<h2>Kmom07/10: Projekt och examination</h2>
HTML;


// Finally, leave it all to the rendering phase of Anjo.
include(ANJO_THEME_PATH);
