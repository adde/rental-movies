<?php
/**
 * This is a Anjo pagecontroller.
 *
 */
// Include the essential config-file which also creates the $anjo variable with its defaults.
include(__DIR__.'/config.php');


// Boot up the table generator
$tableGenerator = new CTableGenerator(['table' => 'Movie']);
$table = $tableGenerator->getAsHTML();
$hitsPerPage = $tableGenerator->getHitsPerPage([2, 4, 8]);
$navigatePage = $tableGenerator->getPageNavigation();

$debug = CDatabase::debug();

// Do it and store it all in variables in the Anjo container.
$anjo['title'] = "Filmarkiv";

$anjo['main'] = <<<HTML
<h1>{$anjo['title']}</h1>

<form>
  <fieldset>
  <legend>Sök</legend>
  <input type=hidden name=hits value='{$tableGenerator->hits}'/>
  <input type=hidden name=page value='1'/>
  <div class="form-group">
    <label for="title">Titel (delsträng, använd % som wildcard):</label>
    <input type="search" class="form-control" name="title" id="title" placeholder="Ange filmtitel" value="{$tableGenerator->title}">
  </div>
  <div class="form-group">
    <label>Skapad mellan åren:</label>
    <div class="row">
      <div class="col-xs-6">
        <input type="text" class="form-control" placeholder="Från" name="year1" value="{$tableGenerator->year1}">
      </div>
      <div class="col-xs-6">
        <input type="text" class="form-control" placeholder="Till" name="year2" value="{$tableGenerator->year2}">
      </div>
    </div>
  </div>
  <div class="form-group">
    <input type='submit' name='submit' class="btn btn-primary" value='Sök'/>
    <a class="btn btn-primary" href='?'>Visa alla</a>
  </div>
  </fieldset>
</form>

<div class='dbtable'>
  <div class='rows'>{$tableGenerator->rows} träffar. {$hitsPerPage}</div>
  <table class="table table-striped">
  {$table}
  </table>
  <div class='pages'>{$navigatePage}</div>
</div>

<hr>

<h3>SQL</h3>
<pre>{$debug}</pre>

HTML;


// Finally, leave it all to the rendering phase of Anjo.
include(ANJO_THEME_PATH);