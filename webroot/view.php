<?php
/**
 * This is a Anjo pagecontroller.
 *
 */
// Include the essential config-file which also creates the $anjo variable with its defaults.
include(__DIR__.'/config.php');


$content = new CContent();

// Get alerts
$alert = (isset($_GET['alert'])) ? urldecode($_GET['alert']) : null;


// Init content database table
if(isset($_GET['init'])) {
  if($content->init()) {
    $alert = '<div class="alert alert-success">Databasen installerades.</div>';
  } else {
    $alert = '<div class="alert alert-danger">Databasen kunde inte installeras.</div>';
  }
}


// Fetch all items
$res = CContent::exists() ? $content->all() : array();


// Create list
$items = null;
if($res) {
  foreach($res AS $key => $val) {
    $items .= "<li>{$val->type} (" . (!$val->available ? 'inte ' : null) . "publicerad): " . htmlentities($val->title, null, 'UTF-8') . " (<a href='edit.php?id={$val->id}'>editera</a> <a href='" . CContent::url($val) . "'>visa</a>)</li>\n";
  }
}


// SQL debug
$debug = CDatabase::debug();


// Do it and store it all in variables in the Anjo container.
$anjo['title'] = "Visa allt innehåll";
$anjo['main'] = <<<HTML

<h1>{$anjo['title']}</h1>

{$alert}

<p>Här är en lista på allt innehåll i databasen.</p>

<ul>
{$items}
</ul>

<p>
  <a class='btn btn-success' href='create.php'>Skapa nytt</a>
  <a class='btn btn-primary' href='blog.php'>Visa alla blogginlägg</a>
  <a class='btn btn-warning' href='view.php?init'>Initiera databasen</a>
</p>

<hr>

<h3>SQL</h3>
<pre>{$debug}</pre>

HTML;


// Finally, leave it all to the rendering phase of Anjo.
include(ANJO_THEME_PATH);
