<?php
/**
 * Config-file for Anjo. Change settings here to affect installation.
 *
 */

/**
 * Set the error reporting.
 *
 */
error_reporting(-1);              // Report all type of errors
ini_set('display_errors', 1);     // Display all errors
ini_set('output_buffering', 0);   // Do not buffer outputs, write directly


/**
 * Set time zone.
 *
 */
date_default_timezone_set('Europe/Stockholm');


/**
 * Define Anjo paths.
 *
 */
define('ANJO_INSTALL_PATH', __DIR__ . '/..');
define('ANJO_THEME_PATH', ANJO_INSTALL_PATH . '/theme/render.php');


/**
 * Specify modules that contains more than one class that needs to be autoloaded
 *
 */
$anjo_autoload_args = [
  'module_paths' => ['DiceGame']
];


/**
 * Include bootstrapping functions.
 *
 */
include(ANJO_INSTALL_PATH . '/src/bootstrap.php');


/**
 * Start the session.
 *
 */
session_name(preg_replace('/[^a-z\d]/i', '', __DIR__));
session_start();


/**
 * Create the Anjo variable.
 *
 */
$anjo = array();


/**
 * Datbase configuration
 *
 */
if( stristr( $_SERVER['SERVER_NAME'], 'd3.nu' ) ) {
  $anjo['database'] = [
    'driver'      => 'mysql',
    'host'        => 'localhost',
    'database'    => 'dbwebb',
    'username'    => 'anjh13',
    'password'    => 'anjh13',
    'charset'     => 'utf8',
    'fetch_mode'  => PDO::FETCH_OBJ
  ];
} else {
  $anjo['database'] = [
    'driver'      => 'mysql',
    'host'        => 'blu-ray.student.bth.se',
    'database'    => 'anjh13',
    'username'    => 'anjh13',
    'password'    => 'y598d2W/',
    'charset'     => 'utf8',
    'fetch_mode'  => PDO::FETCH_OBJ
  ];
}


/**
 * Connect to a MySQL database using PHP PDO
 *
 */
CDatabase::connect($anjo['database']);


/**
 * Site wide settings.
 *
 */
$anjo['lang']         = 'sv';
$anjo['title_append'] = ' | RM Rental Movies';

$anjo['header'] = <<<HTML
<i class="fa fa-flag"></i>
<span class='sitetitle'>adde - oophp</span>
<span class='siteslogan'>Kurs i databaser och objektorienterad PHP-programmering</span>
<span class='collection-page'><a href="http://www.student.bth.se/~anjh13" title="anjh3 samlingssida dbwebb"><i class="fa fa-sitemap"></i></a></span>
HTML;

$auth = CUser::isAuthenticated() ? ' | <a href="logout.php"><i class="fa fa-sign-out"></i> Logga ut</a>' : ' | <a href="login.php"><i class="fa fa-sign-in"></i> Logga in</a>';

$anjo['footer'] = <<<HTML
<span class='sitefooter'>
  Copyright &copy; Andreas Jönsson | 
  <a href='http://validator.w3.org/unicorn/check?ucn_uri=referer&amp;ucn_task=conformance'><i class="fa fa-html5"></i> Unicorn</a>
  {$auth}
</span>
HTML;


$blog = new CBlog();
$anjo['blog_posts'] = $blog->getLatestsPosts(3);


$anjo['navbar'] = array(
  'start'     => array(
    'title'     => '<i class="fa fa-home fa-lg"></i>',
    'url'       => 'index.php'
  ),
  'news'      => array(
    'title'     => 'Nyheter',
    'url'       => 'blog.php'
  ),
  'movies'    => array(
    'title'     => 'Filmer',
    'url'       => 'movies.php'
  ),
  'about'     => array(
    'title'     => 'Om',
    'url'       => 'about.php'
  ),
  'dicegame'  => array(
    'title'     => 'Tävling',
    'url'       => 'dicegame.php'
  ),
);

if(CUser::isAuthenticated()) {
  $anjo['navbar']['admin'] = array(
    'title' => 'Admin <span class="caret"></span>',
    'url' => 'view.php',

    'submenu' => array(
      'create' => array(
        'title'  => 'Skapa sida/inlägg',
        'url'   => 'create.php'
      ),
      'edit' => array(
        'title'  => 'Editera sida/inlägg',
        'url'   => 'view.php'
      ),
    )
  );
}


/**
 * Theme related settings.
 *
 */
$anjo['stylesheets'] = array(
  '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css',
  'css/yeti.css',
  'css/style.css',
);
$anjo['favicon']    = 'favicon.ico';



/**
 * Settings for JavaScript.
 *
 */
$anjo['modernizr'] = 'js/modernizr.js';
$anjo['jquery'] = '//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js';
//$anjo['jquery'] = null; // To disable jQuery
$anjo['javascript_include'] = array(
  '//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js',
  'js/main.js',
);



/**
 * Google analytics.
 *
 */
$anjo['google_analytics'] = null; // Set to null to disable google analytics