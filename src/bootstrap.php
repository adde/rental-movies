<?php
/**
 * Bootstrapping functions, essential and needed for Anjo to work together with some common helpers.
 *
 */

/**
 * Default exception handler.
 *
 */
function myExceptionHandler($exception) {
  echo "Anjo: Uncaught exception: <p>" . $exception->getMessage() . "</p><pre>" . $exception->getTraceAsString(), "</pre>";
}
set_exception_handler('myExceptionHandler');


/**
 * Autoloader for classes.
 *
 */
function myAutoloader($class) {
  global $anjo_autoload_args;

  $path = ANJO_INSTALL_PATH . "/src/{$class}/{$class}.php";
  if(is_file($path)) {
    require_once($path);
  }
  elseif(count($anjo_autoload_args['module_paths']) > 0) {
    array_push($anjo_autoload_args['module_paths'], $class);
    foreach ($anjo_autoload_args['module_paths'] as $p) {
      $path = ANJO_INSTALL_PATH . "/src/{$p}/{$class}.php";
      if(is_file($path)) {
        require_once($path);
      }
    }
  } else {
    throw new Exception("Classfile '{$class}' does not exists.");
  }
}
spl_autoload_register('myAutoloader');
