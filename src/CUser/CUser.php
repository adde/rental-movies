<?php

class CUser {

  public static function login($user, $password) {
    //loggar in användaren om användare och lösenord stämmer.
    $user = CDatabase::select("SELECT acronym, name FROM User WHERE acronym = ? AND password = md5(concat(?, salt))", array($user, $password));
    if(isset($user[0])) {
      $_SESSION['user'] = $user[0];
      return true;
    }
    return false;
  }


  public static function logout() {
    //loggar ut användaren.
    unset($_SESSION['user']);
  }


  public static function isAuthenticated() {
    //returnerar true om användaren är inloggad, annars false.
    if(isset($_SESSION['user'])) {
      return true;
    }
    return false;
  }


  public static function getAcronym() {
    //returnera användarens akronym.
    return $_SESSION['user']->acronym;
  }


  public static function getName() {
    //returnera användarens namn.
    return $_SESSION['user']->name;
  }

}