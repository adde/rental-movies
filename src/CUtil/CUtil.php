<?php

class CUtil {

  /**
   * Dump object with pre-tags
   *
   * @param mixed $obj to debug.
   */
  public static function dump($obj) {
    echo '<pre>' . htmlentities(print_r($obj, true)) . '</pre>';
  }

  /**
   * Create a slug of a string, to be used as url.
   *
   * @param string $str the string to format as slug.
   * @return str the formatted slug.
   */
  public static function slugify($str) {
    $str = mb_strtolower(trim($str));
    $str = str_replace(array('å','ä','ö'), array('a','a','o'), $str);
    $str = preg_replace('/[^a-z0-9-]/', '-', $str);
    $str = trim(preg_replace('/-+/', '-', $str), '-');
    return $str;
  }

  public static function sanitize($str) {
    // Sanitize content before using it.
    return htmlentities($str, null, 'UTF-8');
  }

  public static function currentUrl() {
    $url = "http";
    $url .= (@$_SERVER["HTTPS"] == "on") ? 's' : '';
    $url .= "://";
    $serverPort = ($_SERVER["SERVER_PORT"] == "80") ? '' :
      (($_SERVER["SERVER_PORT"] == 443 && @$_SERVER["HTTPS"] == "on") ? '' : ":{$_SERVER['SERVER_PORT']}");
    $url .= $_SERVER["SERVER_NAME"] . $serverPort . htmlspecialchars($_SERVER["REQUEST_URI"]);
    return $url;
  }

  public static function truncate($text, $numb) {
    if (strlen($text) > $numb) {
      $text = substr($text, 0, $numb);
      $text = substr($text,0,strrpos($text," "));
      $etc = " ...";
      $text = $text.$etc;
    }
    return $text;
  }



}