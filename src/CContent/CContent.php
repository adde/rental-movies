<?php

class CContent {

  public function __construct() {
    //
  }

  /**
   * Initialize the database table.
   *
   * @return boolean if table was initialized successfully.
   */
  public function init() {
    $sql = file_get_contents(__DIR__ . '/content.sql');
    CDatabase::query($sql);
    return true;
  }

  /**
   * Save content to the database.
   *
   * @param array $data with content data to store.
   * @return boolean if content was stored successfully.
   */
  public function store($data) {
    $data = $this->prepareData($data);
    return CDatabase::insert("INSERT INTO Content(title,slug,url,data,author,type,filter,published,created,updated) VALUES(?,?,?,?,?,?,?,?,NOW(),NOW())", $data);
  }

  /**
   * Update content in the database.
   *
   * @param array $data with content data to update.
   * @return boolean if content was updated successfully.
   */
  public function edit($data) {
    $data = $this->prepareData($data, true);
    return CDatabase::update("UPDATE Content SET title = ?, slug = ?, url = ?, data = ?, author = ?, type = ?, filter = ?, published = ?, updated = NOW() WHERE id = ?",$data);
  }

  /**
   * Delete content from the database.
   *
   * @param integer $id of content to delete.
   * @param boolean $permanent if content should be deleted permnantly.
   * @return integer number of rows affected.
   */
  public function destroy($id, $permanent = false) {
    if($permanent)
      return CDatabase::delete("DELETE FROM Content WHERE id = ?", array($id));
    else
      return CDatabase::update("UPDATE Content SET deleted = NOW() WHERE id = ?", array($id));
  }

  /**
   * Get content by id.
   *
   * @param integer $id of content to find.
   * @return content object.
   */
  public function find($field, $value) {
    $res = CDatabase::select("SELECT * FROM Content WHERE {$field} = ? AND published <= NOW()", array($value));
    if(isset($res[0]))
      return $res[0];
    else
      return false;
  }

  /**
   * Get all content from the database.
   *
   * @return array of content objects.
   */
  public function all() {
    return CDatabase::select("SELECT *, (published <= NOW()) AS available FROM Content");
  }

  /**
   * Create a link to the content, based on its type.
   *
   * @param object $content to link to.
   * @return string with url to display content.
   */
  public static function url($content) {
    switch($content->type) {
      case 'page': return "page.php?url={$content->url}"; break;
      case 'post': return "blog.php?slug={$content->slug}"; break;
      default: return null; break;
    }
  }

  /**
   * Sanitize content object before using it.
   *
   * @param object $obj to sanitize.
   * @return object $obj sanitized.
   */
  public static function sanitize($obj) {
    $obj->title     = CUtil::sanitize($obj->title);
    $obj->slug      = CUtil::sanitize($obj->slug);
    $obj->url       = CUtil::sanitize($obj->url);
    $obj->data      = CUtil::sanitize($obj->data);
    $obj->type      = CUtil::sanitize($obj->type);
    $obj->filter    = CUtil::sanitize($obj->filter);
    $obj->published = CUtil::sanitize($obj->published);
    return $obj;
  }

  /**
   * Prepare data for database.
   *
   * @param array $data to prepare.
   * @param boolean $update if its an update or create
   * @return array $data prepared.
   */
  private function prepareData($data, $update = false) {
    $dataPrep = array();
    $dataPrep[] = isset($data['title']) ? $data['title'] : null;
    $dataPrep[] = isset($data['title']) ? CUtil::slugify($data['title']) : null; // slug
    $dataPrep[] = (isset($data['url']) && $data['url'] != '') ? strip_tags($data['url']) : null;
    $dataPrep[] = isset($data['data']) ? $data['data'] : null;
    $dataPrep[] = CUser::getName();
    $dataPrep[] = isset($data['type']) ? strip_tags($data['type']) : null;
    $dataPrep[] = isset($data['filter']) ? implode(',', $data['filter']) : null;
    $dataPrep[] = isset($data['published']) ? strip_tags($data['published']) : null;
    if($update) {
      $dataPrep[] = isset($data['id']) ? strip_tags($data['id']) : null;
      if(!is_numeric($data['id'])) throw new Exception('ID needs to be numeric');
    }
    return $dataPrep;
  }

  public static function exists() {
    $stmt = CDatabase::query("SHOW TABLES LIKE 'Content';");
    if(count($stmt->fetchAll()) > 0)
      return true;
    return false;
  }

}