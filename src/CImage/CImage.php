<?php

class CImage {



  // Image information
  private static $_image = null;
  private static $_width = null;
  private static $_height = null;
  private static $_cacheFileName = null;
  private static $_fileExtension = null;
  private static $_filesize = null;
  private static $_cropWidth = null;
  private static $_cropHeight = null;



  // Default options
  private static $_args = array(
    'verbose'       => false,
    'src'           => null,
    'pathToImage'   => null,
    'saveAs'        => null,
    'quality'       => 60,
    'ignoreCache'   => false,
    'cropToFit'     => false,
    'sharpen'       => false,
    'newWidth'      => null,
    'newHeight'     => null,
    'maxWidth'      => 2000,
    'maxHeight'     => 2000,
  );



  // Put everything together and DO IT!
  public static function make($args) {
    self::$_args = array_intersect_key($args + self::$_args, self::$_args);
    self::_validate();
    self::_startVerbose();
    self::_info();
    self::_calculateDimensions();
    self::_createFilename();
    self::_outputExisting();
    self::_open();
    self::resize();
    self::sharpen();
    self::save();
    self::output();
  }



  private static function _validate() {
    // Validate incoming arguments
    is_dir(IMG_PATH) or errorMessage('The image dir is not a valid directory.');
    is_writable(CACHE_PATH) or self::errorMessage('The cache dir is not a writable directory.');
    isset(self::$_args['src']) or self::errorMessage('Must set src-attribute.');
    preg_match('#^[a-z0-9A-Z-_\.\/]+$#', self::$_args['src']) or self::errorMessage('Filename contains invalid characters.');
    substr_compare(IMG_PATH, self::$_args['pathToImage'], 0, strlen(IMG_PATH)) == 0 or self::errorMessage('Security constraint: Source image is not directly below the directory IMG_PATH.');
    is_null(self::$_args['saveAs']) or in_array(self::$_args['saveAs'], array('png', 'gif', 'jpg', 'jpeg')) or self::errorMessage('Not a valid extension to save image as');
    is_null(self::$_args['quality']) or (is_numeric(self::$_args['quality']) and self::$_args['quality'] > 0 and self::$_args['quality'] <= 100) or self::errorMessage('Quality out of range');
    is_null(self::$_args['newWidth']) or (is_numeric(self::$_args['newWidth']) and self::$_args['newWidth'] > 0 and self::$_args['newWidth'] <= self::$_args['maxWidth']) or self::errorMessage('Width out of range');
    is_null(self::$_args['newHeight']) or (is_numeric(self::$_args['newHeight']) and self::$_args['newHeight'] > 0 and self::$_args['newHeight'] <= self::$_args['maxHeight']) or self::errorMessage('Height out of range');
    is_null(self::$_args['cropToFit']) or (self::$_args['cropToFit'] and self::$_args['newWidth'] and self::$_args['newHeight']) or self::errorMessage('Crop to fit needs both width and height to work');
    return true;
  }



  private static function _info() {
    // Get information on the image
    $imgInfo = list($width, $height, $type, $attr) = getimagesize(self::$_args['pathToImage']);
    !empty($imgInfo) or self::errorMessage("The file doesn't seem to be an image.");
    $mime = $imgInfo['mime'];

    self::$_width = $width;
    self::$_height = $height;

    if(self::$_args['verbose']) {
      self::$_filesize = filesize(self::$_args['pathToImage']);
      self::_verbose("Image file: " . self::$_args['pathToImage']);
      self::_verbose("Image information: " . print_r($imgInfo, true));
      self::_verbose("Image width x height (type): {$width} x {$height} ({$type}).");
      self::_verbose("Image file size: " . self::$_filesize . " bytes.");
      self::_verbose("Image mime type: {$mime}.");
    }
  }



  private static function _calculateDimensions() {
    // Calculate new width and height for the image
    $aspectRatio = self::$_width / self::$_height;

    if(self::$_args['cropToFit'] && self::$_args['newWidth'] && self::$_args['newHeight']) {
      $targetRatio = self::$_args['newWidth'] / self::$_args['newHeight'];
      self::$_cropWidth   = $targetRatio > $aspectRatio ? self::$_width : round(self::$_height * $targetRatio);
      self::$_cropHeight  = $targetRatio > $aspectRatio ? round(self::$_width  / $targetRatio) : self::$_height;
      if(self::$_args['verbose']) { self::_verbose("Crop to fit into box of " . self::$_args['newWidth'] . "x" . self::$_args['newHeight'] . " Cropping dimensions: " . self::$_cropWidth . "x" . self::$_cropHeight); }
    }
    else if(self::$_args['newWidth'] && !self::$_args['newHeight']) {
      self::$_args['newHeight'] = round(self::$_args['newWidth'] / $aspectRatio);
      if(self::$_args['verbose']) { self::_verbose("New width is known " . self::$_args['newWidth'] . ", height is calculated to " . self::$_args['newHeight']); }
    }
    else if(!self::$_args['newWidth'] && self::$_args['newHeight']) {
      self::$_args['newWidth'] = round(self::$_args['newHeight'] * $aspectRatio);
      if(self::$_args['verbose']) { self::_verbose("New height is known " . self::$_args['newHeight'] . ", width is calculated to " . self::$_args['newWidth']); }
    }
    else if(self::$_args['newWidth'] && self::$_args['newHeight']) {
      $ratioWidth  = self::$_width  / self::$_args['newWidth'];
      $ratioHeight = self::$_height / self::$_args['newHeight'];
      $ratio = ($ratioWidth > $ratioHeight) ? $ratioWidth : $ratioHeight;
      self::$_args['newWidth']  = round(self::$_width  / $ratio);
      self::$_args['newHeight'] = round(self::$_height / $ratio);
      if(self::$_args['verbose']) { self::_verbose("New width & height is requested, keeping aspect ratio results in " . self::$_args['newWidth'] . "x" . self::$_args['newHeight']); }
    }
    else {
      self::$_args['newWidth'] = self::$_width;
      self::$_args['newHeight'] = self::$_height;
      if(self::$_args['verbose']) { self::_verbose("Keeping original width & heigth."); }
    }
  }


  private static function _createFilename() {
    // Creating a filename for the cache
    $parts          = pathinfo(self::$_args['pathToImage']);
    self::$_fileExtension  = $parts['extension'];
    self::$_args['saveAs'] = is_null(self::$_args['saveAs']) ? self::$_fileExtension : self::$_args['saveAs'];
    $quality_       = is_null(self::$_args['quality']) ? null : "_q" . self::$_args['quality'];
    $cropToFit_     = is_null(self::$_args['cropToFit']) ? null : "_cf";
    $sharpen_       = is_null(self::$_args['sharpen']) ? null : "_s";
    $dirName        = preg_replace('/\//', '-', dirname(self::$_args['src']));
    $cacheFileName = CACHE_PATH . "-{$dirName}-{$parts['filename']}_" . self::$_args['newWidth'] . "_" . self::$_args['newHeight'] . "{$quality_}{$cropToFit_}{$sharpen_}." . self::$_args['saveAs'];
    $cacheFileName = preg_replace('/^a-zA-Z0-9\.-_/', '', $cacheFileName);

    self::$_cacheFileName = $cacheFileName;

    if(self::$_args['verbose']) { self::_verbose("Cache file is: " . self::$_cacheFileName); }
  }



  private static function _outputExisting() {
    // Is there already a valid image in the cache directory, then use it and exit
    $imageModifiedTime = filemtime(self::$_args['pathToImage']);
    $cacheModifiedTime = is_file(self::$_cacheFileName) ? filemtime(self::$_cacheFileName) : null;

    // If cached image is valid, output it.
    if(!self::$_args['ignoreCache'] && is_file(self::$_cacheFileName) && $imageModifiedTime < $cacheModifiedTime) {
      if(self::$_args['verbose']) { self::_verbose("Cache file is valid, output it."); }
      self::output();
    }

    if(self::$_args['verbose']) { self::_verbose("Cache is not valid, process image and create a cached version of it."); }
  }



  private static function _open() {
    // Open up the original image from file
    if(self::$_args['verbose']) { self::_verbose("File extension is: " . self::$_fileExtension); }

    switch(self::$_fileExtension) {
      case 'jpg':
      case 'jpeg':
        self::$_image = imagecreatefromjpeg(self::$_args['pathToImage']);
        if(self::$_args['verbose']) { self::_verbose("Opened the image as a JPEG image."); }
        break;

      case 'png':
        self::$_image = imagecreatefrompng(self::$_args['pathToImage']);
        if(self::$_args['verbose']) { self::_verbose("Opened the image as a PNG image."); }
        break;

      case 'gif':
        self::$_image = imagecreatefromgif(self::$_args['pathToImage']);
        if(self::$_args['verbose']) { self::_verbose("Opened the image as a GIF image."); }
        break;

      default: errorPage('No support for this file extension.');
    }
  }



  public static function resize() {
    // Resize the image if needed
    if(self::$_args['cropToFit']) {
      if(self::$_args['verbose']) { self::_verbose("Resizing, crop to fit."); }
      $cropX = round((self::$_width - self::$_cropWidth) / 2);
      $cropY = round((self::$_height - self::$_cropHeight) / 2);
      $imageResized = self::_createImageKeepTransparency(self::$_args['newWidth'], self::$_args['newHeight']);
      imagecopyresampled($imageResized, self::$_image, 0, 0, $cropX, $cropY, self::$_args['newWidth'], self::$_args['newHeight'], self::$_cropWidth, self::$_cropHeight);
      self::$_image = $imageResized;
      self::$_width = self::$_args['newWidth'];
      self::$_height = self::$_args['newHeight'];
    }
    else if(!(self::$_args['newWidth'] == self::$_width && self::$_args['newHeight'] == self::$_height)) {
      if(self::$_args['verbose']) { self::_verbose("Resizing, new height and/or width."); }
      $imageResized = self::_createImageKeepTransparency(self::$_args['newWidth'], self::$_args['newHeight']);
      imagecopyresampled($imageResized, self::$_image, 0, 0, 0, 0, self::$_args['newWidth'], self::$_args['newHeight'], self::$_width, self::$_height);
      self::$_image  = $imageResized;
      self::$_width  = self::$_args['newWidth'];
      self::$_height = self::$_args['newHeight'];
    }
  }



  /**
   * Create new image and keep transparency
   *
   * @param resource $image the image to apply this filter on.
   * @return resource $image as the processed image.
   */
  private static function _createImageKeepTransparency($width, $height) {
      $img = imagecreatetruecolor($width, $height);
      imagealphablending($img, false);
      imagesavealpha($img, true);
      return $img;
  }



  public static function sharpen($sharpen = false) {
    // Apply filters and postprocessing of image
    if(self::$_args['sharpen'] || $sharpen) {
      self::$_image = self::_sharpenImage(self::$_image);
    }
  }



  /**
   * Sharpen image as http://php.net/manual/en/ref.image.php#56144
   * http://loriweb.pair.com/8udf-sharpen.html
   *
   * @param resource $image the image to apply this filter on.
   * @return resource $image as the processed image.
   */
  private static function _sharpenImage($image) {
    $matrix = array(
      array(-1,-1,-1,),
      array(-1,16,-1,),
      array(-1,-1,-1,)
    );
    $divisor = 8;
    $offset = 0;
    imageconvolution($image, $matrix, $divisor, $offset);
    return $image;
  }



  public static function save() {
    // Save the image
    switch(self::$_args['saveAs']) {
      case 'jpeg':
      case 'jpg':
        if(self::$_args['verbose']) { self::_verbose("Saving image as JPEG to cache using quality = " . self::$_args['quality'] . "."); }
        imagejpeg(self::$_image, self::$_cacheFileName, self::$_args['quality']);
        break;

      case 'png':
        if(self::$_args['verbose']) { self::_verbose("Saving image as PNG to cache."); }
        imagealphablending(self::$_image, false);
        imagesavealpha(self::$_image, true);
        imagepng(self::$_image, self::$_cacheFileName);
        break;

      case 'gif':
        if(self::$_args['verbose']) { self::_verbose("Saving image as GIF to cache."); }
        imagegif(self::$_image, self::$_cacheFileName);
        break;

      default:
        self::errorMessage('No support to save as this file extension.');
        break;
    }

    if(self::$_args['verbose']) {
      clearstatcache();
      $cacheFilesize = filesize(self::$_cacheFileName);
      self::_verbose("File size of cached file: {$cacheFilesize} bytes.");
      self::_verbose("Cache file has a file size of " . round($cacheFilesize/self::$_filesize*100) . "% of the original size.");
    }
  }



  /**
   * Output an image together with last modified header.
   *
   * @param string $file as path to the image.
   * @param boolean $verbose if verbose mode is on or off.
   */
  public static function output() {
    $file = self::$_cacheFileName;
    $info = getimagesize($file);
    !empty($info) or self::errorMessage("The file doesn't seem to be an image.");
    $mime   = $info['mime'];

    $lastModified = filemtime($file);
    $gmdate = gmdate("D, d M Y H:i:s", $lastModified);

    if(self::$_args['verbose']) {
      self::_verbose("Memory peak: " . round(memory_get_peak_usage() /1024/1024) . "M");
      self::_verbose("Memory limit: " . ini_get('memory_limit'));
      self::_verbose("Time is {$gmdate} GMT.");
    }

    if(!self::$_args['verbose']) header('Last-Modified: ' . $gmdate . ' GMT');
    if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $lastModified){
      if(self::$_args['verbose']) { self::_verbose("Would send header 304 Not Modified, but its verbose mode."); exit; }
      header('HTTP/1.0 304 Not Modified');
    } else {
      if(self::$_args['verbose']) { self::_verbose("Would send header to deliver image with modified time: {$gmdate} GMT, but its verbose mode."); exit; }
      header('Content-type: ' . $mime);
      readfile($file);
    }
    exit;
  }



  /**
   * Display error message.
   *
   * @param string $message the error message to display.
   */
  public static function errorMessage($message) {
    header("Status: 404 Not Found");
    die('img.php says 404 - ' . htmlentities($message));
  }



  /**
   * Display log message.
   *
   * @param string $message the log message to display.
   */
  private static function _verbose($message) {
    echo "<p>" . htmlentities($message) . "</p>";
  }



  private static function _startVerbose() {
    //
    // Start displaying log if verbose mode & create url to current image
    //
    if(self::$_args['verbose']) {
      $query = array();
      parse_str($_SERVER['QUERY_STRING'], $query);
      unset($query['verbose']);
      $url = '?' . http_build_query($query);


      echo <<<HTML
<html lang='en'>
<meta charset='UTF-8'/>
<title>img.php verbose mode</title>
<h1>Verbose mode</h1>
<p><a href=$url><code>$url</code></a><br>
<img src='{$url}' /></p>
HTML;
    }
  }

}