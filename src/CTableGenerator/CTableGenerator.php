<?php

class CTableGenerator {

  public $title;
  public $hits;
  public $page;
  public $year1;
  public $year2;
  public $orderby;
  public $order;
  public $rows = null;

  private $_args = null;


  public function __construct($args) {
    $this->_args = $args;
    $this->_setParameters();
    $this->_checkParameters();
  }


  private function _setParameters() {
    // Get parameters
    $this->title    = isset($_GET['title']) ? $_GET['title'] : null;
    $this->hits     = isset($_GET['hits'])  ? $_GET['hits']  : 8;
    $this->page     = isset($_GET['page'])  ? $_GET['page']  : 1;
    $this->year1    = isset($_GET['year1']) && !empty($_GET['year1']) ? $_GET['year1'] : null;
    $this->year2    = isset($_GET['year2']) && !empty($_GET['year2']) ? $_GET['year2'] : null;
    $this->orderby  = isset($_GET['orderby']) ? strtolower($_GET['orderby']) : 'id';
    $this->order    = isset($_GET['order'])   ? strtolower($_GET['order'])   : 'asc';
  }

  public function setRows($sql = null, $params = array()) {
    if($sql) {
      $sql = "SELECT COUNT(id) AS rows FROM ({$sql}) as Movie";
      $res = CDatabase::select($sql, $params);
      $this->rows = $res[0]->rows;
    }
  }

  public function getMax() {
    // Get max pages for current query, for navigation
    $rows = $this->rows;
    $max = ceil($rows / $this->hits);
    return $max;
  }


  public function getAsHTML() {
    // Put results into a HTML-table
    $res = $this->_buildQuery();
    $tr = "<tr><th>Rad</th><th>Id " . $this->_orderby('id') . "</th><th>Bild</th><th>Titel " . $this->_orderby('title') . "</th><th>År " . $this->_orderby('year') . "</th></tr>";
    foreach($res AS $key => $val) {
      $tr .= "<tr><td>{$key}</td><td>{$val->id}</td><td><img width='80' height='40' src='{$val->image}' alt='{$val->title}' /></td><td>{$val->title}</td><td>{$val->year}</td></tr>";
    }
    return $tr;
  }


  public function getPageNavigation($min=1) {
    $nav = '<ul class="pagination">';
    $nav .= ($this->page != $min) ? "<li><a href='" . $this->_getQueryString(array('page' => $min)) . "'>&lt;&lt;</a></li>" : '<li class="disabled"><a href="#">&lt;&lt;</a></li>';
    $nav .= ($this->page > $min) ? "<li><a href='" . $this->_getQueryString(array('page' => ($this->page > $min ? $this->page - 1 : $min) )) . "'>&lt;</a></li>" : '<li class="disabled"><a href="#">&lt;</a></li>';

    for($i=$min; $i<=$this->getMax(); $i++) {
      if($this->page == $i) {
        $nav .= "<li class='active'><a href='" . $this->_getQueryString(array('page' => $i)) . "'>$i</a></li>";
      }
      else {
        $nav .= "<li><a href='" . $this->_getQueryString(array('page' => $i)) . "'>$i</a></li>";
      }
    }

    $nav .= ($this->page < $this->getMax()) ? "<li><a href='" . $this->_getQueryString(array('page' => ($this->page < $this->getMax() ? $this->page + 1 : $this->getMax()) )) . "'>&gt;</a></li>" : '<li class="disabled"><a href="#">&gt;</a></li>';
    $nav .= ($this->page != $this->getMax()) ? "<li><a href='" . $this->_getQueryString(array('page' => $this->getMax())) . "'>&gt;&gt;</a></li>" : '<li class="disabled"><a href="#">&gt;&gt;</a></li>';
    $nav .= '</ul>';
    return $nav;
  }


  public function getHitsPerPage($hits) {
    $nav = "Träffar per sida: ";
    foreach($hits AS $val) {
      if($this->hits == $val) {
        $nav .= "$val ";
      }
      else {
        $nav .= "<a href='" . $this->_getQueryString(array('hits' => $val)) . "'>$val</a> ";
      }
    }
    return $nav;
  }


  private function _orderby($column) {
    $nav  = "<a href='" . $this->_getQueryString(array('orderby'=>$column, 'order'=>'asc')) . "'><i class='fa fa-sort-asc'></i></a>";
    $nav .= " <a href='" . $this->_getQueryString(array('orderby'=>$column, 'order'=>'desc')) . "'><i class='fa fa-sort-desc'></i></a>";
    return "<span class='orderby'>" . $nav . "</span>";
  }


  private function _checkParameters() {
    // Check that incoming parameters are valid
    is_numeric($this->hits) or die('Check: Hits must be numeric.');
    is_numeric($this->page) or die('Check: Page must be numeric.');
    is_numeric($this->year1) || !isset($this->year1)  or die('Check: Year must be numeric or not set.');
    is_numeric($this->year2) || !isset($this->year2)  or die('Check: Year must be numeric or not set.');
  }


  private function _getQueryString($options=array(), $prepend='?') {
    // parse query string into array
    $query = array();
    parse_str($_SERVER['QUERY_STRING'], $query);

    // Modify the existing query string with new options
    $query = array_merge($query, $options);

    // Return the modified querystring
    return $prepend . htmlentities(http_build_query($query));
  }


  private function _buildQuery() {
    // Prepare the query based on incoming arguments
    $sql      = "SELECT * FROM {$this->_args['table']}";
    $where    = null;
    $limit    = null;
    $sort     = " ORDER BY {$this->orderby} {$this->order}";
    $params   = array();

    // Select by title
    if($this->title) {
      $where .= ' AND title LIKE ?';
      $params[] = $this->title;
    }

    // Select by year
    if($this->year1) {
      $where .= ' AND year >= ?';
      $params[] = $this->year1;
    }
    if($this->year2) {
      $where .= ' AND year <= ?';
      $params[] = $this->year2;
    }

    // Pagination
    if($this->hits && $this->page) {
      $limit = " LIMIT {$this->hits} OFFSET " . (($this->page - 1) * $this->hits);
    }

    // Complete the sql statement
    $where = $where ? " WHERE 1 {$where}" : null;

    // Set number of rows
    $this->setRows($sql . $where, $params);

    $sql = $sql . $where . $sort . $limit;
    $res = CDatabase::select($sql, $params);

    return $res;
  }

}