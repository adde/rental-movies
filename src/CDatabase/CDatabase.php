<?php
/**
* Database
*
* Basic database helper class that creates an integration with the database through PDO.
*
* @author Andreas Jönsson adde90@gmail.com
*
*/

class CDatabase {

  private static $_pdo = null;
  private static $_sql = null;

  /**
   * Connect to database
   *
   */
  public static function connect($args) {
    self::$_sql = array();
    try {
      $driver = 'mysql';
      $charset = 'utf8';
      if($args['driver']) $driver = $args['driver'];
      if($args['charset']) $charset = $args['charset'];
      self::$_pdo = new PDO($driver . ':host=' . $args['host'] . ';dbname=' . $args['database'] . ';charset=' . $charset, $args['username'], $args['password']);
      if($args['fetch_mode']) self::$_pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, $args['fetch_mode']);
    } catch (PDOException $e) {
      throw new Exception('Error connecting to the database: ' . $e->getMessage());
    }
  }

  /**
   * Disconnect from database
   *
   */
  public static function disconnect($args) {
    self::$_pdo = null;
    self::$_sql = null;
  }

  /**
   * Get stuff from table
   *
   */
  public static function select($sql, $args = array()) {
    self::$_sql[] = $sql;
    $stmt = self::$_pdo->prepare($sql);
    if($stmt->execute($args)) {
      return $stmt->fetchAll();
    } else {
      self::errorHandler($stmt);
    }
  }

  /**
   * Insert stuff into table
   *
   */
  public static function insert($sql, $args) {
    self::$_sql[] = $sql;
    $stmt = self::$_pdo->prepare($sql);
    if($stmt->execute($args)) {
      return true;
    } else {
      self::errorHandler($stmt);
    }
  }
  /**
   * Update stuff in table
   *
   */
  public static function update($sql, $args) {
    self::$_sql[] = $sql;
    $stmt = self::$_pdo->prepare($sql);
    if($stmt->execute($args)) {
      return $stmt->rowCount();
    } else {
      self::errorHandler($stmt);
    }
  }

  /**
   * Delete stuff from table
   *
   */
  public static function delete($sql, $args) {
    self::$_sql[] = $sql;
    $stmt = self::$_pdo->prepare($sql);
    if($stmt->execute($args)) {
      return $stmt->rowCount();
    } else {
      self::errorHandler($stmt);
    }
  }

  /**
   * Random query
   *
   */
  public static function query($sql, $args = array()) {
    self::$_sql[] = $sql;
    $stmt = self::$_pdo->prepare($sql);
    if($stmt->execute($args)) {
      return $stmt;
    } else {
      self::errorHandler($stmt);
    }
  }

  /**
   * Take care of errors and throw exceptions
   *
   */
  private static function errorHandler($stmt) {
    $errorInfo = ($stmt) ? $stmt->errorInfo() : self::$_pdo->errorInfo();
    throw new Exception('MySQL Error: ' . $errorInfo[2]);
  }

  /**
   * Return the PDO object
   *
   */
  public static function getPdo() {
    return self::$_pdo;
  }

  /**
   * Return all SQL queries(as a string) that were made during the request
   *
   */
  public static function debug() {
    $sql = self::$_sql;
    $sqlString = '';
    foreach ($sql as $val) {
      $sqlString .= $val . "\n\n";
    }
    $sqlString = rtrim($sqlString, "\n\n");
    return $sqlString;
  }
}