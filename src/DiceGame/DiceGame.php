<?php

class DiceGame {

  /**
   * Properties
   *
   */
  public $round;



  /**
   * Constructor
   *
   */
  public function __construct() {
    $this->init();
  }



  /**
   * Init
   *
   */
  public function init() {
    if(isset($_SESSION['round'])) {
      $this->round = $_SESSION['round'];
    } else {
      $this->round = new Round();
      $_SESSION['round'] = $this->round;
    }
  }



  /**
   * Roll the dice
   *
   */
  public function play() {
    return $this->round->roll()->get_total();
  }



  /**
   * Securing points
   *
   */
  public function secure() {
    return $this->round->secure();
  }



  /**
   * Check if player have won
   *
   */
  public function won($points_to_win) {
    if($this->round->get_total() >= $points_to_win) {
      return true;
    }
    return false;
  }



  /**
   * Reset the current round
   *
   */
  public function reset() {
    $this->round = null;
    $_SESSION['round'] = null;
    $this->init();
  }
}