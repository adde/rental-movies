<?php

class Round {

  /**
   * Properties
   *
   */
  private $points;
  private $points_secured;
  private $current_roll;
  private $rolls;



  /**
   * Constructor
   *
   */
  public function __construct() {
    $this->points = 0;
    $this->points_secured = 0;
    $this->rolls = 0;
  }



  /**
   * Roll the dice one time
   *
   */
  public function roll() {
    $dice = new Dice();
    $this->current_roll = $dice->roll()->get_last_roll();
    $this->rolls += 1;
    if($this->current_roll == 1) {
      if($this->points_secured > 0) {
        $this->points = $this->points_secured;
      }
      else {
        $this->points = $this->current_roll;
      }
    }
    else {
      $this->points += $this->current_roll;
    }
    return $this;
  }



  /**
   * Save current points
   *
   */
  public function secure() {
    $this->points_secured = $this->get_total();
    return $this->points_secured;
  }



  public function get_total() {
    return $this->points;
  }



  public function get_secured_points() {
    return ($this->points_secured != 0) ? $this->points_secured : '-';
  }



  public function get_current_roll() {
    return $this->current_roll;
  }



  public function get_total_rolls() {
    return $this->rolls;
  }

}