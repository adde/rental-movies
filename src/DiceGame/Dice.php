<?php

class Dice {

  /**
   * Properties
   *
   */
  private $lastRoll = array();



  /**
   * Constructor
   *
   */
  public function __construct() {

  }



  /**
   * Roll the dice
   *
   */
  public function roll($times = 1) {
    $this->lastRoll = array();
    for($i = 0; $i < $times; $i++) {
      $this->lastRoll[] = rand(1, 6);
    }
    return $this;
  }



  /**
   * Get the array that contains the last roll(s).
   *
   */
  public function get_results() {
    return $this->lastRoll;
  }



  /**
   * Get the total from the last roll(s).
   *
   */
  public function get_last_roll() {
    return array_sum($this->lastRoll);
  }

}
