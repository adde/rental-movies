<?php

class CNavigation {

  public static function render($navbar) {
    echo '<ul class="nav navbar-nav">';
    foreach ($navbar as $item) {
      $dropdown = (isset($item['submenu'])) ? 'dropdown' : null;
      $dropdown_anchor = (isset($item['submenu'])) ? 'class="dropdown-toggle" data-toggle="dropdown"' : null;
      $selected = (basename($_SERVER['REQUEST_URI']) == $item['url']) ? 'active' : null;
      echo '<li class="'.$dropdown.' '.$selected.'"><a '.$dropdown_anchor.' href="'.$item['url'].'">'.$item['title'].'</a>';
      if(!is_null($dropdown)) {
        echo '<ul class="dropdown-menu">';
        foreach ($item['submenu'] as $subitem) {
          echo '<li><a href="'.$subitem['url'].'">'.$subitem['title'].'</a></li>';
        }
        echo '</ul>';
      }
      echo '</li>';
    }
    echo '</ul>';
  }

  public static function renderDynamic() {
    $pages = new CPage();
    $items = $pages->allPages();
    $nav = '<nav class="navbar navbar-inverse" role="navigation"><ul class="nav navbar-nav">';
    foreach ($items as $item) {
      $selected = (isset($_GET['url']) && $_GET['url'] == $item->url) ? 'active' : null;
      $nav .= '<li class="'.$selected.'"><a href="page.php?url='.$item->url.'">'.CUtil::sanitize($item->title).'</a></li>';
    }
    $nav .= '</ul></nav>';
    echo $nav;
  }

}