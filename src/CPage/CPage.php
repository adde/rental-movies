<?php

class CPage extends CContent {

  private $_page = null;

  public function __construct() {
    parent::__construct();
  }

  public function get($url = null) {
    $this->_page = $this->find('url', $url);
    return $this->_page;
  }

  public function getData() {
    $data = CUtil::sanitize($this->_page->data);
    $data = CTextFilter::make($data, $this->_page->filter);
    return $data;
  }

  public function getTitle() {
    return CUtil::sanitize($this->_page->title);
  }

  public function allPages() {
    return CDatabase::select("SELECT *, (published <= NOW()) AS available FROM Content WHERE type = 'page'");
  }

}