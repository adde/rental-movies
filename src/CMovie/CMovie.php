<?php

class CMovie {

  public function __construct() {
    //
  }

  /**
   * Initialize the database table.
   *
   * @return boolean if table was initialized successfully.
   */
  public function init() {
    $sql = file_get_contents(__DIR__ . '/movie.sql');
    CDatabase::query($sql);
    return true;
  }

  /**
   * Save content to the database.
   *
   * @param array $data with content data to store.
   * @return boolean if content was stored successfully.
   */
  public function store($data) {
    $data = $this->prepareData($data);
    return CDatabase::insert("INSERT INTO Movie(title,director,length,year,plot,image,synopsis) VALUES(?,?,?,?,?,?,?)", $data);
  }

  /**
   * Update content in the database.
   *
   * @param array $data with content data to update.
   * @return boolean if content was updated successfully.
   */
  public function edit($data) {
    $data = $this->prepareData($data, true);
    return CDatabase::update("UPDATE Movie SET title = ?, slug = ?, url = ?, data = ?, author = ?, type = ?, filter = ?, published = ?, updated = NOW() WHERE id = ?",$data);
  }

  /**
   * Delete content from the database.
   *
   * @param integer $id of content to delete.
   * @param boolean $permanent if content should be deleted permnantly.
   * @return integer number of rows affected.
   */
  public function destroy($id) {
    return CDatabase::delete("DELETE FROM Movie WHERE id = ?", array($id));
  }

  /**
   * Get content by id.
   *
   * @param integer $id of content to find.
   * @return content object.
   */
  public function find($field, $value) {
    $res = CDatabase::select("SELECT * FROM Movie WHERE {$field} = ?", array($value));
    if(isset($res[0]))
      return $res[0];
    else
      return false;
  }

  /**
   * Get all content from the database.
   *
   * @return array of content objects.
   */
  public function all() {
    return CDatabase::select("SELECT * FROM Movie");
  }

  /**
   * Prepare data for database.
   *
   * @param array $data to prepare.
   * @param boolean $update if its an update or create
   * @return array $data prepared.
   */
  private function prepareData($data, $update = false) {
    $dataPrep = array();
    $dataPrep[] = isset($data['title']) ? $data['title'] : null;
    $dataPrep[] = isset($data['title']) ? CUtil::slugify($data['title']) : null; // slug
    $dataPrep[] = (isset($data['url']) && $data['url'] != '') ? strip_tags($data['url']) : null;
    $dataPrep[] = isset($data['data']) ? $data['data'] : null;
    $dataPrep[] = CUser::getName();
    $dataPrep[] = isset($data['type']) ? strip_tags($data['type']) : null;
    $dataPrep[] = isset($data['filter']) ? implode(',', $data['filter']) : null;
    $dataPrep[] = isset($data['published']) ? strip_tags($data['published']) : null;
    if($update) {
      $dataPrep[] = isset($data['id']) ? strip_tags($data['id']) : null;
      if(!is_numeric($data['id'])) throw new Exception('ID needs to be numeric');
    }
    return $dataPrep;
  }

  public static function exists() {
    $stmt = CDatabase::query("SHOW TABLES LIKE 'Movie';");
    if(count($stmt->fetchAll()) > 0)
      return true;
    return false;
  }

}