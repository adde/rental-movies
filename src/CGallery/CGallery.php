<?php

class CGallery {



  public static function get($path) {
    $path = self::_getRealPath($path);

    self::_validate($path);

    if(is_dir($path)) {
      $gallery = self::all($path);
    }
    else if(is_file($path)) {
      $gallery = self::item($path);
    }

    return $gallery;
  }



  private static function _validate($path) {
    // Validate incoming arguments
    is_dir(GALLERY_PATH) or errorMessage('The gallery dir is not a valid directory.');
    substr_compare(GALLERY_PATH, $path, 0, strlen(GALLERY_PATH)) == 0 or errorMessage('Security constraint: Source gallery is not directly below the directory GALLERY_PATH.');
  }



  /**
   * Display error message.
   *
   * @param string $message the error message to display.
   */
  private static function errorMessage($message) {
    header("Status: 404 Not Found");
    die('gallery.php says 404 - ' . htmlentities($message));
  }



  /**
   * Read directory and return all items in a ul/li list.
   *
   * @param string $path to the current gallery directory.
   * @param array $validImages to define extensions on what are considered to be valid images.
   * @return string html with ul/li to display the gallery.
   */
  public static function all($path, $validImages = array('png', 'jpg', 'jpeg')) {
    $files = glob($path . '/*');
    $gallery = "<div class='row'>\n";
    $len = strlen(GALLERY_PATH);

    foreach($files as $file) {
      $parts = pathinfo($file);

      // Is this an image or a directory
      if(is_file($file) && in_array($parts['extension'], $validImages)) {
        $item    = "<img src='img.php?src=" . GALLERY_BASEURL . substr($file, $len + 1) . "&amp;width=135&amp;height=135&amp;crop-to-fit' alt=''/>";
        $caption = basename($file);
      }
      elseif(is_dir($file)) {
        $item    = "<img src='img/folder.png' alt=''/>";
        $caption = basename($file) . '/';
      }
      else {
        continue;
      }

      // Avoid to long captions breaking layout
      $fullCaption = $caption;
      if(strlen($caption) > 18) {
        $caption = substr($caption, 0, 10) . '…' . substr($caption, -5);
      }

      $href = substr($file, $len + 1);
      $gallery .= "<div class='col-xs-4 col-md-2'><a class='thumbnail' href='?path={$href}' title='{$fullCaption}'>{$item}<div class='caption'>{$caption}</div></a></div>\n";
    }
    $gallery .= "</div>\n";

    return $gallery;
  }



  /**
   * Read and return info on choosen item.
   *
   * @param string $path to the current gallery item.
   * @param array $validImages to define extensions on what are considered to be valid images.
   * @return string html to display the gallery item.
   */
  public static function item($path, $validImages = array('png', 'jpg', 'jpeg')) {
    $parts = pathinfo($path);
    if(!(is_file($path) && in_array($parts['extension'], $validImages))) {
      return "<p>This is not a valid image for this gallery.";
    }

    // Get info on image
    $imgInfo = list($width, $height, $type, $attr) = getimagesize($path);
    $mime = $imgInfo['mime'];
    $gmdate = gmdate("D, d M Y H:i:s", filemtime($path));
    $filesize = round(filesize($path) / 1024);

    // Get constraints to display original image
    $displayWidth  = $width > 800 ? "&amp;width=800" : null;
    $displayHeight = $height > 600 ? "&amp;height=600" : null;

    // Display details on image
    $len = strlen(GALLERY_PATH);
    $href = GALLERY_BASEURL . substr($path, $len + 1);
    $item = <<<HTML
<p><img src='img.php?src={$href}{$displayWidth}{$displayHeight}' alt=''/></p>
<p>Original image dimensions are {$width}x{$height} pixels. <a href='img.php?src={$href}'>View original image</a>.</p>
<p>File size is {$filesize}KBytes.</p>
<p>Image has mimetype: {$mime}.</p>
<p>Image was last modified: {$gmdate} GMT.</p>
HTML;

    return $item;
  }



  /**
   * Create a breadcrumb of the gallery query path.
   *
   * @param string $path to the current gallery directory.
   * @return string html with ul/li to display the thumbnail.
   */
  public static function breadcrumbs($path) {
    $path = self::_getRealPath($path);
    $parts = explode('/', trim(substr($path, strlen(GALLERY_PATH) + 1), '/'));
    $breadcrumb = "<ul class='breadcrumb'>\n<li><a href='?'>Galleri</a></li>\n";

    if(!empty($parts[0])) {
      $combine = null;
      foreach($parts as $part) {
        $combine .= ($combine ? '/' : null) . $part;
        $breadcrumb .= "<li><a href='?path={$combine}'>$part</a> </li>\n";
      }
    }

    $breadcrumb .= "</ul>\n";
    return $breadcrumb;
  }



  private static function _getRealPath($path) {
    return realpath(GALLERY_PATH . DIRECTORY_SEPARATOR . $path);
  }



}