<?php

class CBlog extends CContent {

  private $_post = null;

  public function __construct() {
    parent::__construct();
  }

  public function get($slug) {
    $this->_post = $this->find('slug', $slug);
    if($this->_post) {
      $this->_post->title  = CUtil::sanitize($this->_post->title);
      $this->_post->data   = CTextFilter::make(CUtil::sanitize($this->_post->data), $this->_post->filter);
    }
    return $this->_post;
  }

  public function getLatestsPosts($limit) {
    if(is_numeric($limit)) {
      $res = CDatabase::select("SELECT * FROM Content WHERE type = 'post' AND published <= NOW() ORDER BY updated DESC LIMIT {$limit}");
      foreach($res as $c) {
        // Sanitize content before using it.
        $c->title  = CUtil::sanitize($c->title);
        $c->data   = CTextFilter::make(CUtil::sanitize($c->data), $c->filter);
      }
      return $res;
    } else
      throw new Exception("Limit needs to be numeric.");
  }
}