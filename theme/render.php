<?php
/**
 * Render content to theme.
 *
 */

// Extract the data array to variables for easier access in the template files.
extract($anjo);

// Include the template functions.
include(__DIR__ . '/functions.php');


// Include the template file.
$template = (isset($template)) ? $template : 'index';
include(__DIR__ . '/'.$template.'.tpl.php');
