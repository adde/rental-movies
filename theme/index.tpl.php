<!doctype html>
<html class="no-js" lang="<?=$lang?>">
<head>
  <meta charset="utf-8">
  <title><?=get_title($title)?></title>
  <?php if(isset($favicon)): ?><link rel="shortcut icon" href="<?=$favicon?>"/><?php endif; ?>
  <?php foreach($stylesheets as $val): ?>
  <link rel="stylesheet" href="<?=$val?>"/>
  <?php endforeach; ?>
  <script src="<?=$modernizr?>"></script>
</head>
<body>

  <div class="container">

    <div class="splash">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <h1><a href="../">Rental Movies</a></h1>
          <p>Hyr dina favoritfilmer online hos oss, smidigt och snabbt!</p>
          <p><a href="movies.php" class="btn btn-primary btn-lg">Gå till akrivet</a></p>
        </div>
      </div>
    </div>

    <div class="navbar navbar-inverse navbar-default">
      <div class="navbar-header">
        <a href="../" class="navbar-brand visible-xs">Rental Movies</a>
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="navbar-collapse collapse" id="navbar-main">
        <?php if(isset($navbar)) CNavigation::render($navbar); ?>
        <form method="post" action="movies.php" class="navbar-form navbar-right">
          <input type="text" name="s" class="form-control" placeholder="Search">
        </form>
      </div>
    </div>

    <div class="row">
      <div class="col-md-8">
        <?=$main;?>
      </div>
      <div class="col-md-4">
        <h2>Senaste nyheter</h2>
        <div class="list-group">
          <?php foreach ($blog_posts as $bp) : ?>
            <a href="blog.php?slug=<?php echo $bp->slug; ?>" class="list-group-item">
              <span class="badge"><?php echo $bp->published; ?></span>
              <?php echo $bp->title; ?>
            </a>
          <?php endforeach; ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <hr>
        <footer class="footer"><?=$footer;?></footer>
      </div>
    </div>

  </div> <!-- end .container -->

<!-- Scripts: -->
<?php if(isset($jquery)):?><script src='<?=$jquery?>'></script><?php endif; ?>

<?php if(isset($javascript_include)): foreach($javascript_include as $val): ?>
<script src='<?=$val?>'></script>
<?php endforeach; endif; ?>

<?php if(isset($google_analytics)): ?>
<script>
  var _gaq=[['_setAccount','<?=$google_analytics?>'],['_trackPageview']];
  (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
  g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
  s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
<?php endif; ?>

</body>
</html>
